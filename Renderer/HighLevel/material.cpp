#include "material.hpp"

#include <stdexcept>

namespace Renderer
{
namespace HighLevel
{
Material::~Material() noexcept
{
	attachedShaders.clear();
}

void Material::AddShader(eastl::string const &name, Shader &&shader) noexcept
{
	attachedShaders.emplace(eastl::make_pair(name, eastl::move(shader)));
	program.AttachShader(attachedShaders[name]);
}

void Material::RemoveShader(eastl::string const &name) noexcept
{
	auto it = attachedShaders.find(name);
	if(it != attachedShaders.end())
	{
		attachedShaders.erase(it);
	}
}

Shader &Material::GetShader(eastl::string const &name)
{
	decltype(attachedShaders)::iterator it = attachedShaders.find(name);
	if(it != attachedShaders.end())
	{
		return it->second;
	}

	eastl::string errorString = eastl::string("Error: Shader with name <") + name +
		"> not found";
	throw std::runtime_error(errorString.c_str());
}

void Material::Compile()
{
	for(auto it = attachedShaders.begin(), end = attachedShaders.end(); it != end; ++it)
	{
		it->second.Compile();
		if(!it->second.Compiled())
		{
			eastl::string errorString = eastl::string("Error: Shader with name <") +
									 it->first + "> failed to compile.\n" +
									 it->second.ErrorLog();
			throw std::runtime_error(errorString.c_str());
		}
	}
}

void Material::Link()
{
	program.Link();
	if(!program.IsValid())
	{
		throw std::runtime_error("Error: Program failed to link or"
								 " is otherwise invalid.");
	}

	program.Use();
}
} // namespace HighLevel
} // namespace Renderer
