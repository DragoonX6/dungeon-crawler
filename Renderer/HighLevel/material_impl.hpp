#ifndef fc0b0ff1_3ef1_4259_a49f_600656f501a3
#define fc0b0ff1_3ef1_4259_a49f_600656f501a3

namespace Renderer
{
namespace HighLevel
{
inline Program &Material::GetProgam() noexcept
{
	return program;
}

inline Texture &Material::GetTexture() noexcept
{
	return texture;
}
} // namespace HighLevel
} // namespace Renderer

#endif // fc0b0ff1_3ef1_4259_a49f_600656f501a3
