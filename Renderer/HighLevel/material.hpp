#ifndef ba589870_bedf_4230_b392_a68c043bfdbe
#define ba589870_bedf_4230_b392_a68c043bfdbe

#include <EASTL/unordered_map.h>
#include <EASTL/string.h>
#include <EASTL/type_traits.h>

#include "../api.hpp"
#include "../OpenGL/Shading/program.hpp"
#include "../OpenGL/Shading/shader.hpp"
#include "../OpenGL/Textures/texture.hpp"

namespace Renderer
{
namespace HighLevel
{
class RENDERER_API Material
{
	static_assert(eastl::is_nothrow_move_constructible<eastl::unordered_map<eastl::string,
			Shader>>::value, "Unordered maps need to be nothrow move constructible");
	static_assert(eastl::is_nothrow_move_assignable<eastl::unordered_map<eastl::string,
			Shader>>::value, "Unordered maps need to be nothrow move assignable");
public:
	Material() = default;
	Material(Material const &) = delete;
	Material(Material &&other) noexcept = default;
	~Material() noexcept;

	Material &operator=(Material const &) = delete;
	Material &operator=(Material &&other) noexcept = default;

	void AddShader(eastl::string const &name, Shader &&shader) noexcept;
	void RemoveShader(eastl::string const &name) noexcept;
	Shader &GetShader(eastl::string const &name);

	inline Program &GetProgam() noexcept;

	inline Texture &GetTexture() noexcept;

	void Compile();
	void Link();

private:
	Program program;
	eastl::unordered_map<eastl::string, Shader> attachedShaders;
	Texture texture;
};
} // namespace HighLevel
} // namespace Renderer

#include "material_impl.hpp"

#endif // ba589870_bedf_4230_b392_a68c043bfdbe
