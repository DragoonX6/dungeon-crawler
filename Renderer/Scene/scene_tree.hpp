#ifndef RENDERER_SCENE_SCENE_TREE_HPP
#define RENDERER_SCENE_SCENE_TREE_HPP

#include <memory>
#include <vector>

#include "../api.hpp"
#include "../Objects/object.hpp"

namespace Renderer
{
	namespace Scene
	{
		using namespace Objects;

		class RENDERER_API SceneTree
		{
		public:
			SceneTree() noexcept;
			SceneTree(SceneTree const &) = delete;
			SceneTree(SceneTree &&other) noexcept = default;
			~SceneTree() noexcept = default;

			SceneTree &operator=(SceneTree const &) = delete;
			SceneTree &operator=(SceneTree &&other) noexcept = default;

			void AddObject(std::unique_ptr<Object> &&object);
			void RemoveObject(Object const &object);

			void Render();

		private:
			std::vector<std::unique_ptr<Object>> tree;
		};
	}
}

#endif // RENDERER_SCENE_SCENE_TREE_HPP
