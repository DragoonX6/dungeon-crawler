#include "scene_tree.hpp"

using namespace Renderer::Scene;

SceneTree::SceneTree() noexcept
{
	tree.reserve(20);
}

void SceneTree::AddObject(std::unique_ptr<Object> &&object)
{
	tree.push_back(std::move(object));
	tree[tree.size() - 1]->index = tree.size() - 1;
}

void SceneTree::RemoveObject(Object const &object)
{
	decltype(tree)::iterator it = tree.begin();
	std::advance(it, static_cast<decltype(tree)::difference_type>(object.index));
	tree.erase(it);

	for(decltype(tree)::size_type i = 0; i < tree.size(); ++i)
	{
		tree[i]->index = i;
	}
}

void SceneTree::Render()
{
	for(std::unique_ptr<Object> &object: tree)
	{
		object->Render();
	}
}
