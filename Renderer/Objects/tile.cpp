#include "tile.hpp"

#include <iostream>
#include <vector>
#include <string>

#include <Common/Helpers/make_array.hpp>
#include <Exceptions/file_exceptions.hpp>

#include "OpenGL/Shading/shader.hpp"

using namespace std::string_literals;

using namespace Common::FileSystem::Exceptions;
using namespace Common::Helpers;

using namespace Renderer::Objects;
using namespace Renderer::HighLevel;

Tile::Tile() noexcept: material(nullptr), ebo(), vao(), vbo()
{
	auto vertexData = make_array(0.f,  0.f,  0.f, 0.f,
								 48.f, 0.f,  1.f, 0.f,
								 48.f, 48.f, 1.f, 1.f,
								 0.f,  48.f, 0.f, 1.f);

	vao.SetActive();

	vbo.SetBufferType(BufferObject::BufferType::array);
	vbo.SetStride<float>(4);
	vbo.SetDataType(BufferObject::DataType::float_);
	vbo.SetActive();
	vbo.UploadData(vertexData, BufferObject::Usage::staticDraw);

	auto elements = make_array(0u, 1u, 2u,
							   2u, 3u, 0u);

	ebo.SetBufferType(BufferObject::BufferType::element);
	ebo.SetStride<gl32core::GLuint>(1);
	ebo.SetDataType(BufferObject::DataType::unsignedInt);
	ebo.SetActive();
	ebo.UploadData(elements, BufferObject::Usage::staticDraw);

	vao.SetInactive();
}

void Tile::SetMaterial(Material *mat) noexcept
{
	material = mat;

	vao.SetActive();

	Program &program = material->GetProgam();

	gl32core::GLint position = program.GetAttribLocation("position");
	vao.VertexAttribPointer(vbo, static_cast<gl32core::GLuint>(position), 2,
							gl32core::GL_FALSE);
	vao.EnableVertexAttributeArray(static_cast<gl32core::GLuint>(position));

	gl32core::GLint textureCoordinates = program.GetAttribLocation("texCoordIn");
	vao.VertexAttribPointer(vbo, static_cast<gl32core::GLuint>(textureCoordinates), 2,
							gl32core::GL_FALSE,
							reinterpret_cast<gl32core::GLvoid*>(5 * vbo.GetStrideSize()));

	vao.SetInactive();
}

void Tile::Render() noexcept
{
	vao.SetActive();

	gl32core::glDrawElements(gl32core::GL_TRIANGLES, ebo.GetSize(),
							 gl32core::GL_UNSIGNED_INT, 0);

	vao.SetInactive();
}
