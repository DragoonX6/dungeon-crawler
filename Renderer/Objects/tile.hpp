#ifndef RENDERER_OBJECTS_TILE_HPP
#define RENDERER_OBJECTS_TILE_HPP

#include "../api.hpp"
#include "../HighLevel/material.hpp"
#include "../OpenGL/Buffers/buffer_object.hpp"
#include "../OpenGL/Buffers/vertex_attribute_array.hpp"

namespace Renderer
{
	namespace Objects
	{
		class RENDERER_API Tile
		{
		public:
			Tile() noexcept;
			Tile(Tile const &) = delete;
			Tile(Tile &&other) noexcept = default;
			~Tile() noexcept = default;

			Tile &operator=(Tile const &) = delete;
			Tile &operator=(Tile &&other) noexcept = default;

			void SetMaterial(HighLevel::Material *mat) noexcept;
			inline HighLevel::Material &GetMaterial() noexcept;

			void Render() noexcept;

		private:
			HighLevel::Material *material;
			BufferObject ebo;
			VertexAttributeArray vao;
			BufferObject vbo;
		};
	}
}

#include "tile_impl.hpp"

#endif // RENDERER_OBJECTS_TILE_HPP
