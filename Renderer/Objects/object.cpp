#include "object.hpp"

#include <Common/Helpers/make_array.hpp>
#include <Common/Exceptions/file_exceptions.hpp>
#include <glbinding/gl32core/types.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/vec4.hpp>

#include "object_types.hpp"

using namespace Common::Helpers;
//using Common::FileSystem::Exceptions::FileException;

namespace Renderer
{
namespace Objects
{
using namespace Renderer::HighLevel;

Object::Object() noexcept: material(), tile(), position(),
	color(), view(), projection(), uniformView(0), uniformProjection(0),
	uniformColor(0), uniformPosition(0), type(TYPE_OBJECT), index(0)
{
}

void Object::Awake() noexcept
{
	Shader vertexShader(Shader::ShaderType::vertex);

	try
	{
		vertexShader.Load("Shaders/test.vert");
	}
	catch(std::runtime_error &e)
	{
		std::cerr << e.what() << std::endl;
	}

	Shader fragmentShader(Shader::ShaderType::fragment);

	try
	{
		fragmentShader.Load("Shaders/test_uniform.frag");
	}
	catch(std::runtime_error &e)
	{
		std::cerr << e.what() << std::endl;
	}

	material.AddShader("fragmentShader", std::move(fragmentShader));
	material.AddShader("vertexShader", std::move(vertexShader));

	try
	{
		material.Compile();
	}
	catch(std::runtime_error &e)
	{
		std::cerr << e.what() << std::endl;
	}

	Program &program = material.GetProgam();
	program.BindFragDataLocation(0, "outColor");

	try
	{
		material.Link();
	}
	catch(std::runtime_error &e)
	{
		std::cerr << e.what() << std::endl;
	}

	uniformColor = program.GetUniformLocation("triangleColor");
	uniformPosition = program.GetUniformLocation("worldPos");

	uniformView = program.GetUniformLocation("view");
	uniformProjection = program.GetUniformLocation("projection");

	view = glm::lookAt(glm::vec3(0.f,0.f,1.f),
					   glm::vec3(0.f,0.f,0.f),
					   glm::vec3(0.f,1.f,0.f));

	projection = glm::ortho(0.f, 1280.f, 720.f, 0.f, 0.f, 100.f);

	material.GetTexture().SetActive();
	material.GetTexture().SetData(0, static_cast<gl32core::GLint>(gl32core::GL_RGB),
								  1, 1, gl32core::GL_RGB, gl32core::GL_FLOAT,
								  make_array(1.0f, 1.0f, 1.0f));

	tile.SetMaterial(&material);
}

void Object::Render() noexcept
{
	Material &mat = tile.GetMaterial();
	mat.GetProgam().Use();

	gl32core::glUniformMatrix4fv(uniformView, 1, gl32core::GL_FALSE,
								 glm::value_ptr(view));
	gl32core::glUniformMatrix4fv(uniformProjection, 1, gl32core::GL_FALSE,
								 glm::value_ptr(projection));
	gl32core::glUniform3f(uniformColor, color.r, color.g, color.b);
	gl32core::glUniform2f(uniformPosition, position.x, position.y);

	mat.GetTexture().SetActive();

	tile.Render();

	mat.GetTexture().SetInactive();
}
} // namespace Objects
} // namespace Renderer
