#ifndef OBJECT_TYPES_HPP
#define OBJECT_TYPES_HPP

#include <Common/types.hpp>

constexpr Common::Types::u8 TYPE_OBJECT = 0;

#endif // OBJECT_TYPES_HPP
