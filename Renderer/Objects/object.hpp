#ifndef RENDERER_OBJECTS_OBJECT
#define RENDERER_OBJECTS_OBJECT

#include <iostream>
#include <vector>

#include <Common/types.hpp>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>

#include "../api.hpp"
#include "../HighLevel/material.hpp"
#include "tile.hpp"

namespace Renderer
{
	namespace Scene
	{
		class SceneTree;
	}

	namespace Objects
	{
		using namespace Common::Types;

		class RENDERER_API Object
		{
		public:
			Object() noexcept;
			Object(Object const &) = delete;
			Object(Object &&other) = default;
			virtual ~Object() noexcept = default;

			Object &operator=(Object const &) = delete;
			Object &operator=(Object &&other) = default;

			inline HighLevel::Material &GetMaterial() noexcept;

			inline Tile &GetTile() noexcept;

			inline glm::vec2 &Position() noexcept;

			inline glm::vec3 &Color() noexcept;

			inline u8 GetType() const noexcept;

			virtual void Awake() noexcept;

			void Render() noexcept;

		protected:
			HighLevel::Material material;
			Tile tile;
			glm::vec2 position;
			glm::vec3 color;
			glm::mat4 view, projection;

			gl32core::GLint uniformView, uniformProjection;
			gl32core::GLint uniformColor;
			gl32core::GLint uniformPosition;

			u8 type;

		private:
			friend class Scene::SceneTree;

			std::vector<Object>::size_type index;
		};
	}
}

#ifndef QT_CREATOR_WORKAROUND
#include "object_impl.hpp"
#endif

#endif // RENDERER_OBJECTS_OBJECT
