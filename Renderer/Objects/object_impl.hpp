#ifndef RENDERER_OBJECTS_OBJECT_IMPL_HPP
#define RENDERER_OBJECTS_OBJECT_IMPL_HPP

namespace Renderer
{
	namespace Objects
	{
		inline HighLevel::Material &Object::GetMaterial() noexcept
		{
			return material;
		}

		inline Tile &Object::GetTile() noexcept
		{
			return tile;
		}

		inline glm::vec2 &Object::Position() noexcept
		{
			return position;
		}

		inline glm::vec3 &Object::Color() noexcept
		{
			return color;
		}
	}
}

#endif // RENDERER_OBJECTS_OBJECT_IMPL_HPP
