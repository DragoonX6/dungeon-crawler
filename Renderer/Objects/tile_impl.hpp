#ifndef RENDERER_OBJECTS_TILE_IMPL_HPP
#define RENDERER_OBJECTS_TILE_IMPL_HPP

namespace Renderer
{
	namespace Objects
	{
		inline HighLevel::Material &Tile::GetMaterial() noexcept
		{
			return *material;
		}
	}
}

#endif // RENDERER_OBJECTS_TILE_IMPL_HPP
