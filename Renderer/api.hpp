#ifndef d95b30b2_a342_4d0f_81cb_1415307d79dd
#define d95b30b2_a342_4d0f_81cb_1415307d79dd

#if defined(RENDERER_DLL) || defined(RENDERER_AS_DLL)
	#ifdef RENDERER_AS_DLL
		#ifdef _WIN32
			#define RENDERER_API __declspec(dllimport)
		#else
			#define RENDERER_API
		#endif
		#define RENDERER_LOCAL
	#else
		#ifdef _WIN32
			#define RENDERER_API __declspec(dllexport)
			#define RENDERER_LOCAL
		#elif (defined(__GNUC__) && (__GNUC__ >= 4))
			#define RENDERER_API __attribute__ ((visibility("default")))
			#define RENDERER_LOCAL __attribute__ ((visibility("hidden")))
		#endif
	#endif
#else
	#define RENDERER_API
	#define RENDERER_LOCAL
#endif

#endif // d95b30b2_a342_4d0f_81cb_1415307d79dd
