#ifndef bf428537_d134_4c8e_a67e_9eb428b71e31
#define bf428537_d134_4c8e_a67e_9eb428b71e31

#include <EASTL/type_traits.h>

#include <Common/Containers/byte_vector.hpp>
#include <Common/Helpers/unique_handle.hpp>
#include <Common/types.hpp>

#include <glbinding/gl32core/enum.h>
#include <glbinding/gl32core/types.h>
#include <glbinding/gl32core/bitfield.h>

#include "../../api.hpp"

namespace Renderer
{
using namespace Common::Types;

class RENDERER_API BufferObject final
{
	using GLEnum = eastl::underlying_type<gl32core::GLenum>::type;

public:
	enum class BufferType: GLEnum
	{
		array             = static_cast<GLEnum>(gl32core::GL_ARRAY_BUFFER),
		copyRead          = static_cast<GLEnum>(gl32core::GL_COPY_READ_BUFFER),
		copyWrite         = static_cast<GLEnum>(gl32core::GL_COPY_WRITE_BUFFER),
		element           = static_cast<GLEnum>(gl32core::GL_ELEMENT_ARRAY_BUFFER),
		pixelPack         = static_cast<GLEnum>(gl32core::GL_PIXEL_PACK_BUFFER),
		pixelUnpack       = static_cast<GLEnum>(gl32core::GL_PIXEL_UNPACK_BUFFER),
		texture           = static_cast<GLEnum>(gl32core::GL_TEXTURE_BUFFER),
		transformFeedback = static_cast<GLEnum>(gl32core::GL_TRANSFORM_FEEDBACK_BUFFER),
		uniform           = static_cast<GLEnum>(gl32core::GL_UNIFORM_BUFFER)
	};

	enum class Usage: GLEnum
	{
		streamDraw  = static_cast<GLEnum>(gl32core::GL_STREAM_DRAW),
		streamRead  = static_cast<GLEnum>(gl32core::GL_STREAM_READ),
		streamCopy  = static_cast<GLEnum>(gl32core::GL_STREAM_COPY),
		staticDraw  = static_cast<GLEnum>(gl32core::GL_STATIC_DRAW),
		staticRead  = static_cast<GLEnum>(gl32core::GL_STATIC_READ),
		staticCopy  = static_cast<GLEnum>(gl32core::GL_STATIC_COPY),
		dynamicDraw = static_cast<GLEnum>(gl32core::GL_DYNAMIC_DRAW),
		dynamicRead = static_cast<GLEnum>(gl32core::GL_DYNAMIC_READ),
		dynamicCopy = static_cast<GLEnum>(gl32core::GL_DYNAMIC_COPY)
	};

public:
	BufferObject() noexcept;
	BufferObject(BufferObject const &) = delete;
	BufferObject(BufferObject &&other) noexcept = default;
	~BufferObject() noexcept;

	BufferObject &operator=(BufferObject const &) = delete;
	BufferObject &operator=(BufferObject &&other) noexcept = default;

	inline void SetBufferType(BufferType type) noexcept;
	inline BufferType GetBufferType() const noexcept;

	inline void SetStride(gl32core::GLint value) noexcept;
	inline gl32core::GLint GetStride() const noexcept;

	inline void SetActive() noexcept;

	template<typename Allocator>
	inline void UploadData(
		Common::Containers::ByteVector<Allocator> const &data,
		Usage usage) noexcept;

	inline gl32core::GLsizei ObjectCount() const noexcept;

private:
	Common::Helpers::UniqueHandle<gl32core::GLuint> buffer;
	gl32core::GLsizei bufferSize;
	BufferType bufferType;
	gl32core::GLint stride;
};
} // namespace Renderer

#include "buffer_object_impl.hpp"

#endif // bf428537_d134_4c8e_a67e_9eb428b71e31
