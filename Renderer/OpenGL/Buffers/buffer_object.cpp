#include "buffer_object.hpp"

namespace Renderer
{
BufferObject::BufferObject() noexcept
: buffer()
, bufferSize(0)
, bufferType(BufferType::array)
, stride(0)
{
	gl32core::glGenBuffers(1, &*buffer);
}

BufferObject::~BufferObject() noexcept
{
	gl32core::glDeleteBuffers(1, &*buffer);
}
} //namespace Renderer
