#ifndef b071d795_b040_4a20_a9bb_25eb631ef498
#define b071d795_b040_4a20_a9bb_25eb631ef498

#include <glbinding/gl32core/functions.h>

namespace Renderer
{
inline void BufferObject::SetBufferType(BufferObject::BufferType type) noexcept
{
	bufferType = type;
}

inline BufferObject::BufferType BufferObject::GetBufferType() const noexcept
{
	return bufferType;
}

inline void BufferObject::SetStride(gl32core::GLint value) noexcept
{
	stride = value;
}

inline gl32core::GLint BufferObject::GetStride() const noexcept
{
	return stride;
}

inline void BufferObject::SetActive() noexcept
{
	gl32core::glBindBuffer(static_cast<gl32core::GLenum>(bufferType), *buffer);
}

template<typename Allocator>
inline void BufferObject::UploadData(
	Common::Containers::ByteVector<Allocator> const &data,
	BufferObject::Usage usage) noexcept
{
	bufferSize = static_cast<gl32core::GLsizei>(data.size());

	gl32core::glBufferData(
		static_cast<gl32core::GLenum>(bufferType),
		static_cast<gl32core::GLsizeiptr>(data.size()),
		data.data(),
		static_cast<gl32core::GLenum>(usage));
}

inline gl32core::GLsizei BufferObject::ObjectCount() const noexcept
{
	return bufferSize / stride;
}
} // namespace Renderer

#endif // b071d795_b040_4a20_a9bb_25eb631ef498
