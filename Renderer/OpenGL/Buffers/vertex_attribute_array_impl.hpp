#ifndef a9c58268_e449_40a6_973f_f66f82557f62
#define a9c58268_e449_40a6_973f_f66f82557f62

namespace Renderer
{
inline uintptr VertexAttributeArray::VertexAttribPointer(
	const BufferObject &vbo,
	gl32core::GLuint index,
	gl32core::GLint size,
	DataType dataType,
	uintptr offset,
	bool normalized) noexcept
{
	gl32core::glVertexAttribPointer(
		index,
		size,
		static_cast<gl32core::GLenum>(dataType),
		normalized ? gl32core::GL_TRUE : gl32core::GL_FALSE,
		vbo.GetStride(),
		reinterpret_cast<gl32core::GLvoid*>(offset));

	uf8 dataSize = 0;

	switch(dataType)
	{
		case DataType::byte:
		case DataType::unsignedByte:
		{
			dataSize = 1;
		} break;
		case DataType::short_:
		case DataType::unsignedShort:
		case DataType::halfFloat:
		{
			dataSize = 2;
		} break;
		case DataType::int_:
		case DataType::unsignedInt:
		case DataType::float_:
		{
			dataSize = 4;
		} break;
		case DataType::double_:
		{
			dataSize = 8;
		} break;
	}

	return uintptr(size * dataSize) + offset;
}

inline void VertexAttributeArray::SetActive() noexcept
{
	gl32core::glBindVertexArray(*vao);
}
inline void VertexAttributeArray::SetInactive() noexcept
{
	gl32core::glBindVertexArray(0);
}

inline void VertexAttributeArray::EnableVertexAttribute(gl32core::GLuint index) noexcept
{
	gl32core::glEnableVertexAttribArray(index);
}

inline void VertexAttributeArray::DisableVertexAttribute(gl32core::GLuint index) noexcept
{
	gl32core::glDisableVertexAttribArray(index);
}

ScopedVertexAttributeArray::ScopedVertexAttributeArray(VertexAttributeArray &vaa) noexcept
: vertexAttribArray(vaa)
{
	vertexAttribArray.SetActive();
}

ScopedVertexAttributeArray::~ScopedVertexAttributeArray() noexcept
{
	vertexAttribArray.SetInactive();
}
} // namespace Renderer

#endif // a9c58268_e449_40a6_973f_f66f82557f62
