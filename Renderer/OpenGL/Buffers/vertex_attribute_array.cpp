#include "vertex_attribute_array.hpp"

namespace Renderer
{
VertexAttributeArray::VertexAttributeArray() noexcept: vao()
{
	gl32core::glGenVertexArrays(1, &*vao);
}

VertexAttributeArray::~VertexAttributeArray() noexcept
{
	gl32core::glDeleteVertexArrays(1, &*vao);
}
} // namespace Renderer
