#ifndef f47dc6c5_81b1_4274_a609_dcd784d0c1d5
#define f47dc6c5_81b1_4274_a609_dcd784d0c1d5

#include <Common/Helpers/unique_handle.hpp>
#include <Common/types.hpp>

#include <glbinding/gl32core/boolean.h>

#include "../../api.hpp"
#include "buffer_object.hpp"

namespace Renderer
{
using namespace Common::Types;

class RENDERER_API VertexAttributeArray final
{
	using GLEnum = eastl::underlying_type<gl32core::GLenum>::type;

public:
	enum class DataType: GLEnum
	{
		byte          = static_cast<GLEnum>(gl32core::GL_BYTE),
		unsignedByte  = static_cast<GLEnum>(gl32core::GL_UNSIGNED_BYTE),
		short_        = static_cast<GLEnum>(gl32core::GL_SHORT),
		unsignedShort = static_cast<GLEnum>(gl32core::GL_UNSIGNED_SHORT),
		int_          = static_cast<GLEnum>(gl32core::GL_INT),
		unsignedInt   = static_cast<GLEnum>(gl32core::GL_UNSIGNED_INT),
		halfFloat     = static_cast<GLEnum>(gl32core::GL_HALF_FLOAT),
		float_        = static_cast<GLEnum>(gl32core::GL_FLOAT),
		double_       = static_cast<GLEnum>(gl32core::GL_DOUBLE)
	};

public:
	VertexAttributeArray() noexcept;
	VertexAttributeArray(VertexAttributeArray const &) = delete;
	VertexAttributeArray(VertexAttributeArray &&other) noexcept = default;
	~VertexAttributeArray() noexcept;

	VertexAttributeArray &operator=(VertexAttributeArray const &) = delete;
	VertexAttributeArray &operator=(VertexAttributeArray &&other) noexcept = default;

	inline uintptr VertexAttribPointer(
		BufferObject const &vbo,
		gl32core::GLuint index,
		gl32core::GLint size,
		DataType dataType,
		uintptr offset = 0,
		bool normalized = false) noexcept;

	inline void SetActive() noexcept;
	inline void SetInactive() noexcept;

	inline void EnableVertexAttribute(gl32core::GLuint index) noexcept;
	inline void DisableVertexAttribute(gl32core::GLuint index) noexcept;

private:
	Common::Helpers::UniqueHandle<gl32core::GLuint> vao;
};

class RENDERER_API ScopedVertexAttributeArray final
{
public:
	ScopedVertexAttributeArray(VertexAttributeArray &vaa) noexcept;
	ScopedVertexAttributeArray(ScopedVertexAttributeArray const &other) = delete;
	ScopedVertexAttributeArray(ScopedVertexAttributeArray &&other) noexcept = delete;
	~ScopedVertexAttributeArray() noexcept;

	ScopedVertexAttributeArray&
	operator=(ScopedVertexAttributeArray const &other) = delete;

	ScopedVertexAttributeArray&
	operator=(ScopedVertexAttributeArray &&other) noexcept = delete;

private:
	VertexAttributeArray &vertexAttribArray;
};
} // namespace Renderer

#include "vertex_attribute_array_impl.hpp"

#endif // f47dc6c5_81b1_4274_a609_dcd784d0c1d5
