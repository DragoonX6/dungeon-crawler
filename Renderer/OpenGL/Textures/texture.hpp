#ifndef b0068135_7e07_4d2b_ae8d_85f832f82555
#define b0068135_7e07_4d2b_ae8d_85f832f82555

#include <EASTL/array.h>
#include <EASTL/vector.h>

#include <Common/Helpers/unique_handle.hpp>
#include <glbinding/gl32core/types.h>

#include "../../api.hpp"

namespace Renderer
{
class RENDERER_API Texture
{
public:
	Texture() noexcept;
	Texture(Texture const &) = delete;
	Texture(Texture &&other) noexcept = default;
	virtual ~Texture() noexcept;

	Texture &operator=(Texture const &) = delete;
	Texture &operator=(Texture &&other) noexcept = default;

	inline void SetActive() noexcept;
	inline void SetInactive() noexcept;

	template<typename T, size_t N, typename =
			 typename eastl::enable_if<eastl::is_fundamental<T>::value>::type>
	inline void SetData(gl32core::GLint mipLevel, gl32core::GLint internalFormat,
						gl32core::GLsizei width, gl32core::GLsizei height,
						gl32core::GLenum format, gl32core::GLenum type,
						eastl::array<T, N> const &data) noexcept;

	template<typename T, typename =
			 typename eastl::enable_if<eastl::is_fundamental<T>::value>::type>
	inline void SetData(gl32core::GLint mipLevel, gl32core::GLint internalFormat,
						gl32core::GLsizei width, gl32core::GLsizei height,
						gl32core::GLenum format, gl32core::GLenum type,
						eastl::vector<T> const &data) noexcept;

private:
	Common::Helpers::UniqueHandle<gl32core::GLuint> texture;
};
} // namespace Renderer

#include "texture_impl.hpp"

#endif // b0068135_7e07_4d2b_ae8d_85f832f82555
