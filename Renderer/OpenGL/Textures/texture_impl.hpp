#ifndef d3824eec_82f3_4224_ba71_8194862539f2
#define d3824eec_82f3_4224_ba71_8194862539f2

#include <glbinding/gl32core/enum.h>
#include <glbinding/gl32core/functions.h>

namespace Renderer
{
inline void Texture::SetActive() noexcept
{
	gl32core::glBindTexture(gl32core::GL_TEXTURE_2D, *texture);
}

inline void Texture::SetInactive() noexcept
{
	gl32core::glBindTexture(gl32core::GL_TEXTURE_2D, 0);
}

template<typename T, size_t N, typename>
inline void Texture::SetData(gl32core::GLint mipLevel, gl32core::GLint internalFormat,
							 gl32core::GLsizei width, gl32core::GLsizei height,
							 gl32core::GLenum format, gl32core::GLenum type,
							 eastl::array<T, N> const &data) noexcept
{
	gl32core::glTexImage2D(gl32core::GL_TEXTURE_2D, mipLevel, internalFormat, width,
						   height, 0, format, type, data.data());
}

template<typename T, typename>
inline void Texture::SetData(gl32core::GLint mipLevel, gl32core::GLint internalFormat,
							 gl32core::GLsizei width, gl32core::GLsizei height,
							 gl32core::GLenum format, gl32core::GLenum type,
							 eastl::vector<T> const &data) noexcept
{
	gl32core::glTexImage2D(gl32core::GL_TEXTURE_2D, mipLevel, internalFormat, width,
							height, 0, format, type, data.data());
}
} // namespace Renderer

#endif // d3824eec_82f3_4224_ba71_8194862539f2
