#include "texture.hpp"

#include <glbinding/gl32core/functions.h>
#include <glbinding/gl32core/enum.h>

namespace Renderer
{
Texture::Texture() noexcept
{
	gl32core::glGenTextures(1, &*texture);
	SetActive();
	gl32core::glTexParameteri(gl32core::GL_TEXTURE_2D, gl32core::GL_TEXTURE_WRAP_S,
							  static_cast<gl32core::GLint>(gl32core::GL_REPEAT));
	gl32core::glTexParameteri(gl32core::GL_TEXTURE_2D, gl32core::GL_TEXTURE_WRAP_T,
							  static_cast<gl32core::GLint>(gl32core::GL_REPEAT));
	gl32core::glTexParameteri(gl32core::GL_TEXTURE_2D, gl32core::GL_TEXTURE_MIN_FILTER,
							  static_cast<gl32core::GLint>(gl32core::GL_LINEAR));
	gl32core::glTexParameteri(gl32core::GL_TEXTURE_2D, gl32core::GL_TEXTURE_MAG_FILTER,
							  static_cast<gl32core::GLint>(gl32core::GL_LINEAR));
}

Texture::~Texture() noexcept
{
	gl32core::glDeleteTextures(1, &*texture);
}
} // namespace Renderer
