#ifndef a291ce96_7e7c_4e6d_bfd3_86b624d68672
#define a291ce96_7e7c_4e6d_bfd3_86b624d68672

#include <EASTL/string.h>

#include <Common/Helpers/unique_handle.hpp>

#include <glbinding/gl32core/boolean.h>
#include <glbinding/gl32core/functions.h>
#include <glbinding/gl32core/types.h>

#include "../../api.hpp"
#include "shader.hpp"

namespace Renderer
{
class RENDERER_API Program
{
public:
	Program() noexcept;
	Program(Program const &) = delete;
	Program(Program &&other) noexcept = default;
	~Program() noexcept;

	Program &operator=(Program const &) = delete;
	Program &operator=(Program &&other) noexcept = default;

	inline void AttachShader(Shader const &shader) noexcept;
	inline void DetachShader(Shader const &shader) noexcept;

	inline void BindFragDataLocation(gl32core::GLuint colorNumber,
									 eastl::string const &outputName) noexcept;

	inline gl32core::GLint GetAttribLocation(eastl::string const &attribName) noexcept;

	inline gl32core::GLint GetUniformLocation(eastl::string const &uniformName) noexcept;

	inline void Link() noexcept;

	inline bool IsValid() noexcept;

	inline void Use() noexcept;

private:
	Common::Helpers::UniqueHandle<gl32core::GLuint> program;
};
} // namespace Renderer

#include "program_impl.hpp"

#endif // a291ce96_7e7c_4e6d_bfd3_86b624d68672
