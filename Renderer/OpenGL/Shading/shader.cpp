#include "shader.hpp"

#include <EASTL/vector.h>

#include <Common/engine.hpp>

namespace Renderer
{
Shader::Shader(Shader::ShaderType shaderType) noexcept: shader()
{
	Create(shaderType);
}

Shader::Shader(Shader::ShaderType shaderType, const eastl::string &fileName): shader()
{
	Create(shaderType);
	Load(fileName);
}

Shader::~Shader() noexcept
{
	Delete();
}

void Shader::Load(const eastl::string &fileName)
{
	using namespace Common::FileSystems;

	FileSystem &fs = Common::Engine::GetFileSystem();
	FileCache &cache = Common::Engine::GetFileCache();

	File *file = nullptr;

	auto cachedFile = cache.Get(fileName);
	if(cachedFile)
	{
		file = &cachedFile.value().get();
	}
	else
	{
		eastl::unique_ptr<File> filePtr = fs.LoadFile(
			fileName,
			File::OpenMode::read | File::OpenMode::shared);
		file = &cache.Add(eastl::move(filePtr));
	}

	eastl::vector<u8> data(static_cast<u64>(file->Size()), 0);
	file->ReadData(data, file->Size());

	data.emplace_back(0);

	char const *const shaderSource = reinterpret_cast<char const *const>(data.data());
	gl32core::glShaderSource(*shader, 1, &shaderSource, nullptr);
}

eastl::string Shader::ErrorLog()
{
	gl32core::GLint logLength;
	glGetShaderiv(*shader, gl32core::GL_INFO_LOG_LENGTH, &logLength);

	eastl::string log(static_cast<eastl::string::size_type>(logLength), 0);

	gl32core::GLsizei readLength;
	gl32core::glGetShaderInfoLog(*shader, logLength, &readLength, &log[0]);

	log.resize(static_cast<eastl::string::size_type>(readLength));

	return log;
}
} // namespace Renderer
