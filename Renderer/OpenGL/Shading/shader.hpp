#ifndef cb713e13_4c28_4970_af8f_87d36a39fae5
#define cb713e13_4c28_4970_af8f_87d36a39fae5

#include <EASTL/string.h>
#include <EASTL/type_traits.h>

#include <Common/Helpers/unique_handle.hpp>
#include <Common/types.hpp>

#include <glbinding/gl32core/boolean.h>
#include <glbinding/gl32core/enum.h>
#include <glbinding/gl32core/functions.h>
#include <glbinding/gl32core/types.h>

#include "../../api.hpp"

namespace Renderer
{
using namespace Common::Types;

class RENDERER_API Shader
{
public:
	enum class ShaderType: typename eastl::underlying_type<gl32core::GLenum>::type
	{
		vertex = static_cast<eastl::underlying_type<gl32core::GLenum>::type>
		(gl32core::GL_VERTEX_SHADER),

		geometry = static_cast<eastl::underlying_type<gl32core::GLenum>::type>
		(gl32core::GL_GEOMETRY_SHADER),

		fragment = static_cast<eastl::underlying_type<gl32core::GLenum>::type>
		(gl32core::GL_FRAGMENT_SHADER)
	};

	Shader() noexcept = default;
	Shader(ShaderType shaderType) noexcept;
	Shader(ShaderType shaderType, eastl::string const &fileName);
	Shader(Shader const&) = delete;
	Shader(Shader &&other) noexcept = default;
	~Shader() noexcept;

	Shader &operator=(Shader const&) = delete;
	Shader &operator=(Shader &&other) noexcept = default;

	inline void Create(ShaderType shaderType) noexcept;
	inline void Delete() noexcept;

	void Load(eastl::string const &fileName);

	inline void Compile() noexcept;
	inline bool Compiled() noexcept;

	eastl::string ErrorLog();

private:
	friend class Program;

	Common::Helpers::UniqueHandle<gl32core::GLuint> shader;
};
} // namespace Renderer

#include "shader_impl.hpp"

#endif // cb713e13_4c28_4970_af8f_87d36a39fae5
