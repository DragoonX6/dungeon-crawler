#ifndef a1a8329d_553b_4503_b8ea_51a8ad7a3c0a
#define a1a8329d_553b_4503_b8ea_51a8ad7a3c0a

namespace Renderer
{
inline void Shader::Create(Shader::ShaderType shaderType) noexcept
{
	*shader = gl32core::glCreateShader(static_cast<gl32core::GLenum>(shaderType));
}

inline void Shader::Delete() noexcept
{
	gl32core::glDeleteShader(*shader);
}

inline void Shader::Compile() noexcept
{
	gl32core::glCompileShader(*shader);
}

inline bool Shader::Compiled() noexcept
{
	gl32core::GLint status;
	glGetShaderiv(*shader, gl32core::GL_COMPILE_STATUS, &status);

	return static_cast<gl32core::GLboolean>(status) == gl32core::GL_TRUE;
}
} // namespace Renderer

#endif // a1a8329d_553b_4503_b8ea_51a8ad7a3c0a
