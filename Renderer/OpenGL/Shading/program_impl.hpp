#ifndef f91a7b44_f9a9_4668_a3f3_7c1bae10c22f
#define f91a7b44_f9a9_4668_a3f3_7c1bae10c22f

namespace Renderer
{
inline void Program::AttachShader(const Shader &shader) noexcept
{
	gl32core::glAttachShader(*program, shader.shader.Get());
}

inline void Program::DetachShader(const Shader &shader) noexcept
{
	gl32core::glDetachShader(*program, shader.shader.Get());
}

inline void Program::BindFragDataLocation(gl32core::GLuint colorNumber,
										  const eastl::string &outputName) noexcept
{
	gl32core::glBindFragDataLocation(*program, colorNumber, outputName.c_str());
}

inline gl32core::GLint Program::GetAttribLocation(const eastl::string &attribName) noexcept
{
	return gl32core::glGetAttribLocation(*program, attribName.c_str());
}

inline gl32core::GLint Program::GetUniformLocation(eastl::string const &uniformName)
noexcept
{
	return gl32core::glGetUniformLocation(*program, uniformName.c_str());
}

inline void Program::Link() noexcept
{
	gl32core::glLinkProgram(*program);
}

inline bool Program::IsValid() noexcept
{
	gl32core::glValidateProgram(*program);

	gl32core::GLint status;
	gl32core::glGetProgramiv(*program, gl32core::GL_VALIDATE_STATUS, &status);

	return static_cast<gl32core::GLboolean>(status) == gl32core::GL_TRUE;
}

inline void Program::Use() noexcept
{
	gl32core::glUseProgram(*program);
}
} // namespace Renderer

#endif // f91a7b44_f9a9_4668_a3f3_7c1bae10c22f
