#include "program.hpp"

namespace Renderer
{
Program::Program() noexcept: program()
{
	*program = gl32core::glCreateProgram();
}

Program::~Program() noexcept
{
	gl32core::glDeleteProgram(*program);
}
} // namespace Renderer
