#include <GLFW/glfw3.h>

#if defined(_WIN32) && defined(UNICODE)
extern "C" int wmain();

extern "C" int wmain()
#else
int main()
#endif
{
	glfwInit();

	glfwSetWindowAttrib(nullptr, 0, 0);

	return 0;
}
