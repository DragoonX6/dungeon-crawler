#ifndef b8e0ddd3_0849_4ceb_819a_ac2ea1fe4c13
#define b8e0ddd3_0849_4ceb_819a_ac2ea1fe4c13

#include <EASTL/unique_ptr.h>

#include <Common/EntityComponent/entity.hpp>
#include <Common/Input/input_controller.hpp>
#include <Common/State/state.hpp>
#include <Common/Windowing/window.hpp>

namespace Game
{
namespace States
{
class TestState : public Common::States::State
{
public:
	TestState() = default;
	TestState(TestState const &) = delete;
	TestState(TestState &&other) noexcept = default;
	~TestState() noexcept override = default;

	TestState &operator=(TestState const &) = delete;
	TestState &operator=(TestState &&other) noexcept = default;

	bool Init() noexcept override;
	void Execute() noexcept override;
	void Exit() noexcept override;
	void Resume() noexcept override;

	void OnExit() noexcept;

private:
	Common::Windowing::Window *window;
	eastl::unique_ptr<Common::Input::InputController> inputController;
};
} // namespace States
} // namespace Game

#endif // b8e0ddd3_0849_4ceb_819a_ac2ea1fe4c13
