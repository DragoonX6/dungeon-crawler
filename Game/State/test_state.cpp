#include "test_state.hpp"

#include <EASTL/utility.h>

#include <glbinding/gl32core/enum.h>
#include <glbinding/gl32core/bitfield.h>
#include <glbinding/gl32core/functions.h>
#include <glm/vec3.hpp>

#include <Common/engine.hpp>

namespace Game
{
namespace States
{
using namespace Common::Input;
using namespace Common::States;
using namespace Common::Windowing;

bool TestState::Init() noexcept
{
	auto optWindow = Common::Engine::GetWindowManager().FindWindow("MainWindow");
	window = &optWindow->get();

	InputListener &listener = window->GetInputListener();
	listener.ShouldCaptureInput(true);
	inputController = eastl::make_unique<InputController>(listener);
	inputController->BindAction<TestState, &TestState::OnExit>
		("Exit", InputController::ButtonState::pressed, this);

	return true;
}

void TestState::Execute() noexcept
{
	while(IsRunning())
	{
		window->GetInputListener().SwapBuffers();
		inputController->PollInput();

		gl32core::glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		gl32core::glClear(gl32core::GL_COLOR_BUFFER_BIT);

//		for(Entity &entity : entities)
//			entity.Update();

		window->SwapBuffers();
	}
}

void TestState::Exit() noexcept
{
	Stop();
}

void TestState::Resume() noexcept
{
}

void TestState::OnExit() noexcept
{
	Stop();
}
} // namespace States
} // namespace Game
