#include "config.hpp"

#include <iostream>
#include <memory>
#include <thread>

#include <EASTL/string.h>
#include <EASTL/utility.h>

#ifdef RENDERER_DEBUG_CONTEXT
#include <glbinding/gl32core/functions.h>
#include <glbinding/gl32ext/enum.h>
#include <glbinding/gl32ext/functions.h>
#endif

#include <glbinding/Binding.h>
#include <GLFW/glfw3.h>

#include <Common/engine.hpp>
#include <Common/FileSystem/Flat/flat_file_system.hpp>
#include <Common/State/state_queue.hpp>
#include <Common/Windowing/window.hpp>

// #include <WaifuMemory/memory_pool.hpp>
// #include <WaifuMemory/memory_hook.hpp>

#if defined(_WIN32) && defined(UNICODE)
#include <utf8.h>
#endif

#include "State/test_state.hpp"

// Turned off because ld got better than I anticipated. And this WaifuMemory will need to be
// linked before all other libs, which is something that I'm not willing to do now.
/* static void __attribute__ ((constructor)) initialize()
{
	using namespace Waifu::Memory;
	MemoryPool::SetPoolAllocationPolicy(
				MemoryPool::AllocationPolicy::FastAllocAlwaysMerge);
	MemoryPool::SetPoolSize(1_Mib);
	MemoryPool::Instance();
	CRTInitialized = true;
}

static void __attribute__ ((destructor)) finish()
{
	CRTInitialized = false;
} */

void GameThread();

void GameThread()
{
	auto currentWindow = Common::Engine::GetWindowManager().FindWindow("MainWindow");
	currentWindow->get().MakeContextCurrent();
	glbinding::Binding::useCurrentContext();

	Common::Engine::GetStateQueue().Execute();
	currentWindow->get().ClearContext();

	currentWindow->get().Destroy();
}

#if defined(_WIN32) && defined(UNICODE)
extern "C" int wmain(int argumentCount, wchar_t *wideArguments[]);

extern "C" int wmain(int argumentCount, wchar_t *wideArguments[])
#else
int main(int argumentCount, char *inArguments[])
#endif
{
	using namespace Common::Input;
	using namespace Common::Windowing;
	using namespace Game::States;

	eastl::vector<eastl::string> arguments;
	arguments.reserve(static_cast<size_t>(argumentCount));

#if defined(_WIN32) && defined(UNICODE)
	for(eastl::string::size_type i = 0;
		i < static_cast<eastl::string::size_type>(argumentCount); ++i)
	{
		size_t length = wcslen(wideArguments[i]);
		arguments.emplace_back(eastl::string());
		utf8::utf16to8(wideArguments[i], wideArguments[i] + length,
					   eastl::back_inserter(arguments[i]));
		std::cout << arguments[i].c_str() << std::endl;
	}
#else
	for(eastl::string::size_type i = 0;
		i < static_cast<eastl::string::size_type>(argumentCount); ++i)
	{
		arguments.emplace_back(eastl::string(inArguments[i]));
	}
#endif
	Common::Engine::SetFileSystem(
				eastl::make_unique<Common::FileSystems::FlatFileSystem>());

	if(argumentCount > 1)
	{
		Common::Engine::GetFileSystem().SetWorkingDirectory(arguments[1]);
	}

	if(!glfwInit())
	{
		std::cerr << "Failed to initialize GLFW." << std::endl;

		return -1;
	}

	glfwSetErrorCallback(&WindowManager::ErrorCallback);

	InputController::BindKey("Exit", GLFW_KEY_ESCAPE);
	InputController::BindKey("Up", GLFW_KEY_W);
	InputController::BindKey("Down", GLFW_KEY_S);
	InputController::BindKey("Left", GLFW_KEY_A);
	InputController::BindKey("Right", GLFW_KEY_D);

	WindowManager &windowManager = Common::Engine::GetWindowManager();
	Window &currentWindow = windowManager.AddWindow("MainWindow");

	Window::WindowConfiguration windowConfig;
	windowConfig.resizeMode = Window::ResizeMode::aspectRatio;
	windowConfig.title = "Waifu";
	windowConfig.width = 1280;
	windowConfig.height = 720;
	windowConfig.windowMode = Window::WindowMode::windowed;
	currentWindow.SetWindowConfiguration(eastl::move(windowConfig));

	try
	{
		currentWindow.Create();
	}
	catch(std::runtime_error &e)
	{
		std::cerr << e.what() << std::endl;

		return -1;
	}

	// Make context current before starting up any state,
	// this is for proper initialization.
	currentWindow.MakeContextCurrent();

	glbinding::Binding::initialize();

#ifdef RENDERER_DEBUG_CONTEXT
	gl32ext::glDebugMessageCallback(WindowManager::DebugCallback, nullptr);
	gl32core::glEnable(gl32ext::GL_DEBUG_OUTPUT);
	gl32core::glEnable(gl32ext::GL_DEBUG_OUTPUT_SYNCHRONOUS_ARB);
#endif

	Common::Engine::GetStateQueue().EnqueueState(eastl::make_unique<TestState>());

	currentWindow.ClearContext();

	std::thread gameThread(&GameThread);

	Common::Engine::GetWindowManager().Run();

	if(gameThread.joinable())
		gameThread.join();

	glfwTerminate();

	return 0;
}
