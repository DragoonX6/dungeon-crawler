#ifndef eb9c4c33_860c_4c20_ab7a_48596c5e39bc
#define eb9c4c33_860c_4c20_ab7a_48596c5e39bc

#include <EABase/config/eaplatform.h>

// Enable discrete GPU on NVidia optimus and AMD XPowerExpress systems
#ifdef EA_PLATFORM_MICROSOFT
#include <windows.h>
#undef FindWindow

extern "C"
{
	__declspec(dllexport) extern int AmdPowerXpressRequestHighPerformance;
	__declspec(dllexport) extern DWORD NvOptimusEnablement;
}
#endif // EA_PLATFORM_MICROSOFT

#endif // eb9c4c33_860c_4c20_ab7a_48596c5e39bc
