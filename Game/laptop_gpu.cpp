#include "laptop_gpu.hpp"

// Enable discrete GPU on NVidia optimus and AMD XPowerExpress systems
#ifdef EA_PLATFORM_MICROSOFT
extern "C"
{
	__declspec(dllexport) int AmdPowerXpressRequestHighPerformance = 1;
	__declspec(dllexport) DWORD NvOptimusEnablement = 0x00000001;
}
#endif // EA_PLATFORM_MICROSOFT
