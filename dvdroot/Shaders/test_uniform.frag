#version 150

in vec2 texCoordFrag;

uniform sampler2D tex;
uniform vec3 triangleColor;

out vec4 outColor;

void main()
{
	outColor = texture(tex, texCoordFrag) * vec4(triangleColor, 1.0);
}
