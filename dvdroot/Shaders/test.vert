#version 150

in vec2 vertexPosition;
in vec2 texCoordIn;

uniform vec2 position;
//uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

out vec2 texCoordFrag;

void main()
{
	texCoordFrag = texCoordIn;
	gl_Position = projection * view * vec4(vertexPosition + position, 0.0, 1.0);
	gl_Position.y *= -1;
}
