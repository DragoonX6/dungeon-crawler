#ifndef d5b94e2c_f702_44b9_9789_ac371d14b25e
#define d5b94e2c_f702_44b9_9789_ac371d14b25e

#if defined(COMMON_DLL) || defined(COMMON_AS_DLL)
	#ifdef COMMON_AS_DLL
		#ifdef _WIN32
			#define COMMON_API __declspec(dllimport)
		#else
			#define COMMON_API
		#endif
		#define COMMON_LOCAL
	#else
		#ifdef _WIN32
			#define COMMON_API __declspec(dllexport)
			#define COMMON_LOCAL
		#elif (defined(__GNUC__) && (__GNUC__ >= 4))
			#define COMMON_API __attribute__ ((visibility("default")))
			#define COMMON_LOCAL __attribute__ ((visibility("hidden")))
		#endif
	#endif
#else
	#define COMMON_API
	#define COMMON_LOCAL
#endif

#endif // d5b94e2c_f702_44b9_9789_ac371d14b25e
