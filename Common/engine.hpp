#ifndef dd03f9ac_2ef3_4683_b0d0_29fe4c20288d
#define dd03f9ac_2ef3_4683_b0d0_29fe4c20288d

#include <EASTL/unique_ptr.h>

#include "api.hpp"
#include "config.hpp"
#include "FileSystem/file_system.hpp"
#include "FileSystem/file_cache.hpp"
#include "State/state_queue.hpp"
#include "Windowing/window_manager.hpp"

namespace Common
{
class COMMON_API Engine final
{
public:
	static FileSystems::FileSystem &GetFileSystem() noexcept;
	static void SetFileSystem(eastl::unique_ptr<FileSystems::FileSystem> &&fs) noexcept;

	static FileSystems::FileCache &GetFileCache() noexcept;

	static Windowing::WindowManager &GetWindowManager() noexcept;

	static States::StateQueue &GetStateQueue() noexcept;

private:
	static eastl::unique_ptr<FileSystems::FileSystem> fileSystem;
	static FileSystems::FileCache fileCache;
	static Windowing::WindowManager windowManager;
	static States::StateQueue stateQueue;
};
}

#endif // dd03f9ac_2ef3_4683_b0d0_29fe4c20288d
