#ifndef bc3d0b3b_ed29_4f59_933d_bac86198468f
#define bc3d0b3b_ed29_4f59_933d_bac86198468f

#include <exception>
#include <EASTL/string.h>

#include "../api.hpp"

namespace Common
{
namespace Exceptions
{
class COMMON_API RuntimeError: public std::exception
{
public:
	explicit RuntimeError(eastl::string const &error);
	explicit RuntimeError(eastl::string &&error) noexcept;
	RuntimeError(RuntimeError const &other) = default;
	RuntimeError(RuntimeError &&other) noexcept = default;
	~RuntimeError() noexcept override;

	RuntimeError &operator=(RuntimeError const &other) = default;
	RuntimeError &operator=(RuntimeError &&other) noexcept = default;

	char const *what() const noexcept override final;

	inline eastl::string Message() const noexcept;

private:
	eastl::string message;
};

inline eastl::string RuntimeError::Message() const noexcept
{
	return message;
}

class COMMON_API InvalidArgument: public RuntimeError
{
public:
	explicit InvalidArgument(eastl::string const &error);
	explicit InvalidArgument(eastl::string &&error) noexcept;
	InvalidArgument(InvalidArgument const &other) = default;
	InvalidArgument(InvalidArgument &&other) noexcept = default;
	~InvalidArgument() noexcept override;

	InvalidArgument &operator=(InvalidArgument const &other) = default;
	InvalidArgument &operator=(InvalidArgument &&other) noexcept = default;
};
} // namespace Exceptions
} // namespace Common

#endif // bc3d0b3b_ed29_4f59_933d_bac86198468f
