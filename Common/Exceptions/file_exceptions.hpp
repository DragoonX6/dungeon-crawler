#ifndef f6eef354_01f4_4d1a_8906_d69eb2d2b818
#define f6eef354_01f4_4d1a_8906_d69eb2d2b818

#include <EASTL/string.h>

#include "../api.hpp"
#include "exceptions.hpp"

namespace Common
{
namespace FileSystems
{
namespace Exceptions
{
class COMMON_API FileException: public Common::Exceptions::RuntimeError
{
public:
	explicit FileException(eastl::string const &error);
	explicit FileException(eastl::string &&error) noexcept;
	FileException(FileException const&) = default;
	FileException(FileException&&) noexcept = default;
	~FileException() noexcept override;

	FileException &operator=(FileException const &) = default;
	FileException &operator=(FileException &&) = default;
};

class COMMON_API InvalidOpenMode final: public FileException
{
public:
	explicit InvalidOpenMode(eastl::string const &error);
	explicit InvalidOpenMode(eastl::string &&error) noexcept;
	InvalidOpenMode(InvalidOpenMode const&) = default;
	InvalidOpenMode(InvalidOpenMode&&) noexcept = default;
	~InvalidOpenMode() noexcept override;

	InvalidOpenMode &operator=(InvalidOpenMode const &) = default;
	InvalidOpenMode &operator=(InvalidOpenMode &&) = default;
};

class COMMON_API EndOfFile final: public FileException
{
public:
	explicit EndOfFile(eastl::string const &error);
	explicit EndOfFile(eastl::string &&error) noexcept;
	EndOfFile(EndOfFile const&) = default;
	EndOfFile(EndOfFile&&) noexcept = default;
	~EndOfFile() noexcept override;

	EndOfFile &operator=(EndOfFile const &) = default;
	EndOfFile &operator=(EndOfFile &&) = default;
};

class COMMON_API IOError final: public FileException
{
public:
	explicit IOError(eastl::string const &error);
	explicit IOError(eastl::string &&error) noexcept;
	IOError(IOError const&) = default;
	IOError(IOError&&) noexcept = default;
	~IOError() noexcept override;

	IOError &operator=(IOError const &) = default;
	IOError &operator=(IOError &&) = default;
};

class COMMON_API BadFileDescriptor final: public FileException
{
public:
	explicit BadFileDescriptor(eastl::string const &error);
	explicit BadFileDescriptor(eastl::string &&error) noexcept;
	BadFileDescriptor(BadFileDescriptor const&) = default;
	BadFileDescriptor(BadFileDescriptor&&) noexcept = default;
	~BadFileDescriptor() noexcept override;

	BadFileDescriptor &operator=(BadFileDescriptor const &) = default;
	BadFileDescriptor &operator=(BadFileDescriptor &&) = default;
};

class COMMON_API AccessDenied final: public FileException
{
public:
	explicit AccessDenied(eastl::string const &error);
	explicit AccessDenied(eastl::string &&error) noexcept;
	AccessDenied(AccessDenied const&) = default;
	AccessDenied(AccessDenied&&) noexcept = default;
	~AccessDenied() noexcept override;

	AccessDenied &operator=(AccessDenied const &) = default;
	AccessDenied &operator=(AccessDenied &&) = default;
};

class COMMON_API FileIsDirectory final: public FileException
{
public:
	explicit FileIsDirectory(eastl::string const &error);
	explicit FileIsDirectory(eastl::string &&error) noexcept;
	FileIsDirectory(FileIsDirectory const&) = default;
	FileIsDirectory(FileIsDirectory&&) noexcept = default;
	~FileIsDirectory() noexcept override;

	FileIsDirectory &operator=(FileIsDirectory const &) = default;
	FileIsDirectory &operator=(FileIsDirectory &&) = default;
};

class COMMON_API SymlinkLoop final: public FileException
{
public:
	explicit SymlinkLoop(eastl::string const &error);
	explicit SymlinkLoop(eastl::string &&error) noexcept;
	SymlinkLoop(SymlinkLoop const&) = default;
	SymlinkLoop(SymlinkLoop&&) noexcept = default;
	~SymlinkLoop() noexcept override;

	SymlinkLoop &operator=(SymlinkLoop const &) = default;
	SymlinkLoop &operator=(SymlinkLoop &&) = default;
};

class COMMON_API TooManyFileDescriptors final: public FileException
{
public:
	explicit TooManyFileDescriptors(eastl::string const &error);
	explicit TooManyFileDescriptors(eastl::string &&error) noexcept;
	TooManyFileDescriptors(TooManyFileDescriptors const&) = default;
	TooManyFileDescriptors(TooManyFileDescriptors&&) noexcept = default;
	~TooManyFileDescriptors() noexcept override;

	TooManyFileDescriptors &operator=(TooManyFileDescriptors const &) = default;
	TooManyFileDescriptors &operator=(TooManyFileDescriptors &&) = default;
};

class COMMON_API FileNameTooLong final: public FileException
{
public:
	explicit FileNameTooLong(eastl::string const &error);
	explicit FileNameTooLong(eastl::string &&error) noexcept;
	FileNameTooLong(FileNameTooLong const&) = default;
	FileNameTooLong(FileNameTooLong&&) noexcept = default;
	~FileNameTooLong() noexcept override;

	FileNameTooLong &operator=(FileNameTooLong const &) = default;
	FileNameTooLong &operator=(FileNameTooLong &&) = default;
};

class COMMON_API TooManyOpenFiles final: public FileException
{
public:
	explicit TooManyOpenFiles(eastl::string const &error);
	explicit TooManyOpenFiles(eastl::string &&error) noexcept;
	TooManyOpenFiles(TooManyOpenFiles const&) = default;
	TooManyOpenFiles(TooManyOpenFiles&&) noexcept = default;
	~TooManyOpenFiles() noexcept override;

	TooManyOpenFiles &operator=(TooManyOpenFiles const &) = default;
	TooManyOpenFiles &operator=(TooManyOpenFiles &&) = default;
};

class COMMON_API FileNotFound final: public FileException
{
public:
	explicit FileNotFound(eastl::string const &error);
	explicit FileNotFound(eastl::string &&error) noexcept;
	FileNotFound(FileNotFound const&) = default;
	FileNotFound(FileNotFound&&) noexcept = default;
	~FileNotFound() noexcept override;

	FileNotFound &operator=(FileNotFound const &) = default;
	FileNotFound &operator=(FileNotFound &&) = default;
};

class COMMON_API OutOfSpace final: public FileException
{
public:
	explicit OutOfSpace(eastl::string const &error);
	explicit OutOfSpace(eastl::string &&error) noexcept;
	OutOfSpace(OutOfSpace const&) = default;
	OutOfSpace(OutOfSpace&&) noexcept = default;
	~OutOfSpace() noexcept override;

	OutOfSpace &operator=(OutOfSpace const &) = default;
	OutOfSpace &operator=(OutOfSpace &&) = default;
};

class COMMON_API InvalidPath final: public FileException
{
public:
	explicit InvalidPath(eastl::string const &error);
	explicit InvalidPath(eastl::string &&error) noexcept;
	InvalidPath(InvalidPath const&) = default;
	InvalidPath(InvalidPath&&) noexcept = default;
	~InvalidPath() noexcept override;

	InvalidPath &operator=(InvalidPath const &) = default;
	InvalidPath &operator=(InvalidPath &&) = default;
};

class COMMON_API FileTooBig final: public FileException
{
public:
	explicit FileTooBig(eastl::string const &error);
	explicit FileTooBig(eastl::string &&error) noexcept;
	FileTooBig(FileTooBig const&) = default;
	FileTooBig(FileTooBig&&) noexcept = default;
	~FileTooBig() noexcept override;

	FileTooBig &operator=(FileTooBig const &) = default;
	FileTooBig &operator=(FileTooBig &&) = default;
};
} // namespace Exceptions
} // namespace FileSystems
} // namespace Common

#endif // f6eef354_01f4_4d1a_8906_d69eb2d2b818
