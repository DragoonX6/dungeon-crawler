#include "exceptions.hpp"

namespace Common
{
namespace Exceptions
{
RuntimeError::RuntimeError(eastl::string const &error)
: message(error)
{
}

RuntimeError::RuntimeError(eastl::string &&error) noexcept
: message(eastl::move(error))
{
}

RuntimeError::~RuntimeError() noexcept = default;

char const *RuntimeError::what() const noexcept
{
	return message.c_str();
}

InvalidArgument::InvalidArgument(eastl::string const &error)
: RuntimeError(error)
{
}

InvalidArgument::InvalidArgument(eastl::string &&error) noexcept
: RuntimeError(eastl::move(error))
{
}

InvalidArgument::~InvalidArgument() noexcept = default;
} // namespace Exceptions
} // namespace Common
