#include "file_exceptions.hpp"

namespace Common
{
namespace FileSystems
{
namespace Exceptions
{
FileException::FileException(eastl::string const &error)
: Common::Exceptions::RuntimeError(error)
{
}

FileException::FileException(eastl::string &&error) noexcept
: Common::Exceptions::RuntimeError(eastl::move(error))
{
}

FileException::~FileException() noexcept = default;

InvalidOpenMode::InvalidOpenMode(eastl::string const &error)
: FileException(error)
{
}

InvalidOpenMode::InvalidOpenMode(eastl::string &&error) noexcept
: FileException(eastl::move(error))
{
}

InvalidOpenMode::~InvalidOpenMode() noexcept = default;

EndOfFile::EndOfFile(const eastl::string &error)
: FileException(error)
{
}

EndOfFile::EndOfFile(eastl::string &&error) noexcept
: FileException(eastl::move(error))
{
}

EndOfFile::~EndOfFile() noexcept = default;

IOError::IOError(const eastl::string &error)
: FileException(error)
{
}

IOError::IOError(eastl::string &&error) noexcept
: FileException(eastl::move(error))
{
}

IOError::~IOError() noexcept = default;

BadFileDescriptor::BadFileDescriptor(const eastl::string &error)
: FileException(error)
{
}

BadFileDescriptor::BadFileDescriptor(eastl::string &&error) noexcept
: FileException(eastl::move(error))
{
}

BadFileDescriptor::~BadFileDescriptor() noexcept = default;

AccessDenied::AccessDenied(const eastl::string &error)
: FileException(error)
{
}

AccessDenied::AccessDenied(eastl::string &&error) noexcept
: FileException(eastl::move(error))
{
}

AccessDenied::~AccessDenied() noexcept = default;

FileIsDirectory::FileIsDirectory(const eastl::string &error)

: FileException(error)
{
}

FileIsDirectory::FileIsDirectory(eastl::string &&error) noexcept
: FileException(eastl::move(error))
{
}

FileIsDirectory::~FileIsDirectory() noexcept = default;

SymlinkLoop::SymlinkLoop(const eastl::string &error)
: FileException(error)
{
}

SymlinkLoop::SymlinkLoop(eastl::string &&error) noexcept
: FileException(eastl::move(error))
{
}

SymlinkLoop::~SymlinkLoop() noexcept = default;

TooManyFileDescriptors::TooManyFileDescriptors(const eastl::string &error)
: FileException(error)
{
}

TooManyFileDescriptors::TooManyFileDescriptors(eastl::string &&error) noexcept
: FileException(eastl::move(error))
{
}

TooManyFileDescriptors::~TooManyFileDescriptors() noexcept = default;

FileNameTooLong::FileNameTooLong(const eastl::string &error)
: FileException(error)
{
}

FileNameTooLong::FileNameTooLong(eastl::string &&error) noexcept
: FileException(eastl::move(error))
{
}

FileNameTooLong::~FileNameTooLong() noexcept = default;

TooManyOpenFiles::TooManyOpenFiles(const eastl::string &error)
: FileException(error)
{
}

TooManyOpenFiles::TooManyOpenFiles(eastl::string &&error) noexcept
: FileException(eastl::move(error))
{
}

TooManyOpenFiles::~TooManyOpenFiles() noexcept = default;

FileNotFound::FileNotFound(const eastl::string &error)
: FileException(error)
{
}

FileNotFound::FileNotFound(eastl::string &&error) noexcept
: FileException(eastl::move(error))
{
}

FileNotFound::~FileNotFound() noexcept = default;

OutOfSpace::OutOfSpace(const eastl::string &error)
: FileException(error)
{
}

OutOfSpace::OutOfSpace(eastl::string &&error) noexcept
: FileException(eastl::move(error))
{
}

OutOfSpace::~OutOfSpace() noexcept = default;

InvalidPath::InvalidPath(const eastl::string &error)
: FileException(error)
{
}

InvalidPath::InvalidPath(eastl::string &&error) noexcept
: FileException(eastl::move(error))
{
}

InvalidPath::~InvalidPath() noexcept = default;

FileTooBig::FileTooBig(const eastl::string &error)
: FileException(error)
{
}

FileTooBig::FileTooBig(eastl::string &&error) noexcept
: FileException(eastl::move(error))
{
}

FileTooBig::~FileTooBig() noexcept = default;
} // namespace Exceptions
} // namespace FileSystems
} // namespace Common
