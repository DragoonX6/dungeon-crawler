#ifndef fcf215fa_0007_415a_8ce7_74e1fbdbc3df
#define fcf215fa_0007_415a_8ce7_74e1fbdbc3df

#include <atomic>
#include <system_error>
#include <thread>

/*
 * spinlock class that might satisfy all the requirements of the Mutex concept.
 * Does satisfy the following concpets:
 * * Lockable
 * * DefaultConstructible
 * * Destructible
 * * not copyable
 * * not moveable
 */
namespace Common
{
namespace Threading
{
class spinlock
{
public:
	spinlock() noexcept
	: current_thread()
	, internal_lock()
	{
		internal_lock.clear();
	}

	spinlock(spinlock const &) = delete;
	spinlock(spinlock &&) noexcept = delete;

	spinlock &operator=(spinlock const &) = delete;
	spinlock &operator=(spinlock &&) noexcept = delete;

	/*
	 * Locks the current spinlock.
	 * Throws: std::system_error with the error code:
	 *			std::errc::resource_deadlock_would_occur
	 * if lock is called twice by the same thread without prior unlocking.
	 */
	void lock()
	{
		if(current_thread == std::this_thread::get_id())
		{
			throw std::system_error(make_error_code(
						std::errc::resource_deadlock_would_occur));
		}

		while(internal_lock.test_and_set(std::memory_order_acq_rel));

		current_thread = std::this_thread::get_id();
	}

	void unlock() noexcept
	{
		internal_lock.clear(std::memory_order_release);
		current_thread = std::thread::id();
	}

	bool try_lock() noexcept
	{
		bool result = !internal_lock.test_and_set(std::memory_order_acq_rel);

		if(result)
			current_thread = std::this_thread::get_id();

		return result;
	}

private:
	std::thread::id current_thread;
	std::atomic_flag internal_lock;
};
} // namespace Threading
} // namespace Common

#endif // fcf215fa_0007_415a_8ce7_74e1fbdbc3df
