#include "state.hpp"

namespace Common
{
namespace States
{
State::State() noexcept
: isRunning(true)
, entities()
, systems()
{
}

State::~State() noexcept = default;
} // namespace States
} // namespace Common
