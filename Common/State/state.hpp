#ifndef bce84eda_be65_4187_b025_53a80d204dd4
#define bce84eda_be65_4187_b025_53a80d204dd4

#include <atomic>

#include <EASTL/optional.h>
#include <EASTL/vector.h>

#include "../EntityComponent/component.hpp"
#include "../EntityComponent/component_system.hpp"
#include "../EntityComponent/entity.hpp"
#include "../api.hpp"

namespace Common
{
namespace States
{
class StateQueue;

class COMMON_API State
{
public:
	State() noexcept;
	State(State const &) = delete;
	State(State &&) noexcept = delete;
	virtual ~State() noexcept;

	State &operator=(State const &) = delete;
	State &operator=(State &&) noexcept = delete;

	inline bool IsRunning() const noexcept;

	virtual bool Init() noexcept = 0;
	virtual void Execute() noexcept = 0;
	virtual void Exit() noexcept = 0;
	virtual void Resume() noexcept = 0;

	inline void Stop() noexcept;

	inline eastl::optional<eastl::reference_wrapper<ECS::ComponentSystem>>
	FindComponentSystem(ECS::ComponentSystem::SystemType systemType);

protected:
	std::atomic_bool isRunning;
	eastl::vector<ECS::Entity> entities;
	eastl::vector<ECS::ComponentSystem> systems;
};
} // namespace States
} // namespace Common

#include "state_impl.hpp"

#endif // bce84eda_be65_4187_b025_53a80d204dd4
