#ifndef ebd57104_f1f5_4143_a7d2_145583f6c533
#define ebd57104_f1f5_4143_a7d2_145583f6c533

namespace Common
{
namespace States
{
inline bool State::IsRunning() const noexcept
{
	return isRunning.load(std::memory_order_acquire);
}

inline void State::Stop() noexcept
{
	isRunning.store(false, std::memory_order_release);
}

inline eastl::optional<eastl::reference_wrapper<ECS::ComponentSystem>>
State::FindComponentSystem(ECS::ComponentSystem::SystemType systemType)
{
	auto it = eastl::find_if(
		systems.begin(),
		systems.end(),
		[systemType](ECS::ComponentSystem const &system)
		{
			return system.Type() == systemType;
		});

	if(it == systems.end())
		return eastl::nullopt;

	return eastl::ref(*it);
}
} // namespace States
} // namespace Common

#endif // ebd57104_f1f5_4143_a7d2_145583f6c533
