#ifndef f178bf29_2d18_4fdb_bfd2_9ad8e9bc4272
#define f178bf29_2d18_4fdb_bfd2_9ad8e9bc4272

namespace Common
{
namespace States
{
inline State &StateQueue::CurrentState()
{
	return *currentState;
}

inline State const &StateQueue::CurrentState() const
{
	return *currentState;
}
} // namespace States
} // namespace Common

#endif // f178bf29_2d18_4fdb_bfd2_9ad8e9bc4272
