#ifndef b7df0c88_c446_437a_95ee_d37408c9a7bb
#define b7df0c88_c446_437a_95ee_d37408c9a7bb

#include <atomic>

#include <EASTL/unique_ptr.h>
#include <EASTL/vector.h>

#include "../api.hpp"
#include "../types.hpp"
#include "state.hpp"

namespace Common
{
using namespace Types;

namespace Windowing
{
class Window;
} // namespace Windowing

namespace States
{
class COMMON_API StateQueue
{
public:
	StateQueue() noexcept = default;
	StateQueue(StateQueue const &other) = delete;
	StateQueue(StateQueue &&other) noexcept = delete;
	~StateQueue() noexcept;

	StateQueue &operator=(StateQueue const &other) = delete;
	StateQueue &operator=(StateQueue &&other) noexcept = delete;

	void EnqueueState(eastl::unique_ptr<State> &&state) noexcept;

	void Execute() noexcept;

	void Exit() noexcept;

	inline State &CurrentState();
	inline State const &CurrentState() const;

private:
	eastl::unique_ptr<State> currentState;
	std::atomic_bool isRunning;
	eastl::vector<eastl::unique_ptr<State>> stateQueue;
};
} // namespace States
} // namespace Common

#include "state_queue_impl.hpp"

#endif // b7df0c88_c446_437a_95ee_d37408c9a7bb
