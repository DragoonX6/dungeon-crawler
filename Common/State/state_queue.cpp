#include "state_queue.hpp"

namespace Common
{
namespace States
{
StateQueue::~StateQueue() noexcept
{
	currentState.reset();

	stateQueue.clear();
}

void StateQueue::EnqueueState(eastl::unique_ptr<State> &&state) noexcept
{
	stateQueue.emplace_back(eastl::move(state));
}

void StateQueue::Execute() noexcept
{
	isRunning.store(true, std::memory_order_relaxed);

	while(stateQueue.size())
	{
		if(!isRunning.load(std::memory_order_acquire))
			break;

		currentState = eastl::move(stateQueue.back());
		stateQueue.pop_back();

		if(!currentState->Init())
			continue;

		currentState->Execute();
		currentState->Exit();
	}
}

void StateQueue::Exit() noexcept
{
	isRunning.store(false, std::memory_order_release);
	currentState->Stop();
}
} // namespace States
} // namespace Common
