#ifndef bf3b0d66_7671_47c2_bf74_c54a503b595a
#define bf3b0d66_7671_47c2_bf74_c54a503b595a

#include <EASTL/functional.h>
#include <EASTL/vector.h>

#include "../api.hpp"
#include "component.hpp"

namespace Common
{
namespace ECS
{
class COMMON_API ComponentSystem
{
public:
	using SystemType = eastl::underlying_type<Component::Type>::type;

public:
	ComponentSystem(SystemType systemType) noexcept;
	ComponentSystem(ComponentSystem const &) = delete;
	ComponentSystem(ComponentSystem &&) noexcept = default;
	virtual ~ComponentSystem() noexcept;

	ComponentSystem &operator=(ComponentSystem const &) = delete;
	ComponentSystem &operator=(ComponentSystem &&) noexcept = default;

	virtual void Update() = 0;

	// Get the system's type.
	inline SystemType Type() const noexcept;

	inline size_t AddComponent();

private:
	SystemType type;
	eastl::vector<Component> components;
};
} // namespace ECS
} // namespace Common

#include "component_system_impl.hpp"

#endif // bf3b0d66_7671_47c2_bf74_c54a503b595a
