#include "component_system.hpp"

namespace Common
{
namespace ECS
{
ComponentSystem::ComponentSystem(SystemType systemType) noexcept
: type(systemType)
, components()
{
}

ComponentSystem::~ComponentSystem() noexcept = default;

} // namespace ECS
} // namespace Common
