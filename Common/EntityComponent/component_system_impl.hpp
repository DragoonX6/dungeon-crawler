#ifndef a3e3a757_1d10_43cc_b6c0_86b362a43825
#define a3e3a757_1d10_43cc_b6c0_86b362a43825

namespace Common
{
namespace ECS
{
inline ComponentSystem::SystemType ComponentSystem::Type() const noexcept
{
	return type;
}

inline size_t ComponentSystem::AddComponent()
{
	components.emplace_back();
	return components.size() - 1;
}

} // namespace ECS
} // namespace Common

#endif // a3e3a757_1d10_43cc_b6c0_86b362a43825
