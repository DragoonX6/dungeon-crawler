#ifndef ea209db6_7258_4afa_bfa6_5b78d0c72c54
#define ea209db6_7258_4afa_bfa6_5b78d0c72c54

#include "../api.hpp"
#include  "../types.hpp"

namespace Common
{
using namespace Types;

namespace ECS
{
struct COMMON_API Component
{
	enum class Type: u8
	{
		Transform,
		BuiltinTypeEnd
	};

	Type type;
};

struct COMMON_API ComponentPtr
{
	Component::Type componentType;
	size_t index;
};
} // namespace ECS
} // namespace Common

#endif // ea209db6_7258_4afa_bfa6_5b78d0c72c54
