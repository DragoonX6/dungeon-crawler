#ifndef d63143ec_6501_4ea7_ac7d_dac743f19885
#define d63143ec_6501_4ea7_ac7d_dac743f19885

#pragma once

#include <EASTL/algorithm.h>
#include <EASTL/type_traits.h>
#include <EASTL/vector.h>

#include "../types.hpp"
#include "../api.hpp"
#include "component.hpp"

namespace Common
{
using namespace Types;

namespace ECS
{
class COMMON_API Entity final
{
	using ComponentType = eastl::underlying_type<Component::Type>::type;

public:
	Entity() noexcept = default;
	Entity(Entity const &) = delete;
	Entity(Entity &&) noexcept = default;
	~Entity() noexcept = default;

	Entity &operator=(Entity const &) = delete;
	Entity &operator=(Entity &&) noexcept = default;

	void AddComponent(ComponentType componentType);
	inline bool hasComponent(ComponentType componentType);

private:
	eastl::vector<ComponentPtr> components;
};
} // namespace ECS
} // namespace Common

#include "entity_impl.hpp"

#endif // d63143ec_6501_4ea7_ac7d_dac743f198851
