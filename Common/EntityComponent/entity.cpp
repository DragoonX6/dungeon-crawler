#include "entity.hpp"

#include <EASTL/algorithm.h>

#include <stdexcept>

#include "../engine.hpp"
#include "../State/state.hpp"
#include "component_system.hpp"

namespace Common
{
namespace ECS
{
void Entity::AddComponent(ComponentType componentType)
{
	if(EA_UNLIKELY(hasComponent(componentType)))
		throw std::runtime_error("Component type already exists.");

	auto componentSystem = Engine::GetStateQueue()
		.CurrentState()
		.FindComponentSystem(componentType);

	if(!componentSystem)
		throw std::runtime_error("No component system known for component type.");

	size_t index = componentSystem->get().AddComponent();

	components.push_back(ComponentPtr{static_cast<Component::Type>(componentType), index});
}
} // namespace ECS
} // namespace Common
