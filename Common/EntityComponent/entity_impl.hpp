#ifndef cdaa4ddf_dc23_4c96_8697_65d6ab0d2e52
#define cdaa4ddf_dc23_4c96_8697_65d6ab0d2e52

namespace Common
{
namespace ECS
{
inline bool Entity::hasComponent(ComponentType componentType)
{
	return eastl::find_if(components.begin(), components.end(),
						  [componentType](ComponentPtr const &componentPtr)
						  {
							  return static_cast<ComponentType>(componentPtr.componentType)
								  == componentType;
						  });
}
} // namespace ECS
} // namespace Common

#endif // cdaa4ddf_dc23_4c96_8697_65d6ab0d2e52
