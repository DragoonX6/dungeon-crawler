#include "engine.hpp"

namespace Common
{
eastl::unique_ptr<FileSystems::FileSystem> Engine::fileSystem;
FileSystems::FileCache Engine::fileCache;
Windowing::WindowManager Engine::windowManager;
States::StateQueue Engine::stateQueue;

FileSystems::FileSystem &Engine::GetFileSystem() noexcept
{
	return *fileSystem;
}

void Engine::SetFileSystem(eastl::unique_ptr<FileSystems::FileSystem> &&fs) noexcept
{
	fileSystem = eastl::move(fs);
}

FileSystems::FileCache &Engine::GetFileCache() noexcept
{
	return fileCache;
}

Windowing::WindowManager &Engine::GetWindowManager() noexcept
{
	return windowManager;
}

States::StateQueue &Engine::GetStateQueue() noexcept
{
	return stateQueue;
}
} //namespace Common
