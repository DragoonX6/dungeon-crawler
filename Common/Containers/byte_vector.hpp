#ifndef d83e5670_e5b2_4664_99eb_cc4b9099365b
#define d83e5670_e5b2_4664_99eb_cc4b9099365b

#include <EASTL/array.h>
#include <EASTL/initializer_list.h>
#include <EASTL/vector.h>

#include "../api.hpp"
#include "../types.hpp"

namespace Common
{
using namespace Types;

namespace Containers
{
// A vector of bytes which allows you to put arbitrary fundamental types in it.
// Mainly for OpenGL vertex data, kind of like how the GPU would handle it.
template<typename Allocator = EASTLAllocatorType>
class COMMON_API ByteVector final: eastl::vector<u8, Allocator>
{
	using base = eastl::vector<u8, Allocator>;
public:
	ByteVector() noexcept = default;

	// Allows you to initialize with an arbitrary amount of copies of a fundamental value.
	template<typename T>
	inline ByteVector(
		typename base::size_type count,
		T const &value,
		const Allocator &alloc = Allocator()) noexcept;

	// Allows you to initalize by making a byte by byte copy of a vector of fundamental
	// values.
	template<typename T>
	explicit inline ByteVector(
		eastl::vector<T> const &vec,
		Allocator const &alloc = EASTLAllocatorType());

	// Allows you to initalize by making a byte by byte copy of an array of fundamental
	// values.
	template<typename T, size_t N>
	explicit inline ByteVector(
		eastl::array<T, N> const &arr,
		Allocator const &alloc = EASTLAllocatorType());

	ByteVector(ByteVector const &other) = default;
	ByteVector(ByteVector &&other) noexcept = default;
	~ByteVector() noexcept = default;

	ByteVector &operator=(ByteVector const &other) = default;
	ByteVector &operator=(ByteVector &&other) noexcept = default;

	// Allows you to overwrite the vector data with count copies of value.
	template<typename T>
	inline void assign(typename base::size_type count, T const &value);

	// Inserts a copy of the value at pos in the vector.
	template<typename T>
	inline typename base::iterator insert(typename base::const_iterator pos, T const &value);

	// Pushes back a byte copy of value in the vector.
	template<typename T>
	inline void push_back(T const &value);

	// Pushes back a byte copy of vec in the vector.
	template<typename T>
	inline void push_back(eastl::vector<T> const &vec);

	// Pushes back a byte copy of arr in the vector.
	template<typename T, size_t N>
	inline void push_back(eastl::array<T, N> const &arr);

	inline void swap(ByteVector &other);
};

template<typename Allocator>
inline bool operator==(ByteVector<Allocator> const &lhs, ByteVector<Allocator> const &rhs);

template<typename Allocator>
inline bool operator!=(ByteVector<Allocator> const &lhs, ByteVector<Allocator> const &rhs);

template<typename Allocator>
inline bool operator<(ByteVector<Allocator> const &lhs, ByteVector<Allocator> const &rhs);

template<typename Allocator>
inline bool operator<=(ByteVector<Allocator> const &lhs, ByteVector<Allocator> const &rhs);

template<typename Allocator>
inline bool operator>(ByteVector<Allocator> const &lhs, ByteVector<Allocator> const &rhs);

template<typename Allocator>
inline bool operator>=(ByteVector<Allocator> const &lhs, ByteVector<Allocator> const &rhs);
} // namespace Containers
} // namespace Common

namespace eastl
{
template<typename Allocator>
void swap(
	Common::Containers::ByteVector<Allocator> &lhs,
	Common::Containers::ByteVector<Allocator> &rhs);
} // namespace eastl

#include "byte_vector_impl.hpp"

#endif // d83e5670_e5b2_4664_99eb_cc4b9099365bg
