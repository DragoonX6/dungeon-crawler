#ifndef ed5753eb_2899_4830_b609_33aeb73910e9
#define ed5753eb_2899_4830_b609_33aeb73910e9

namespace Common
{
namespace Containers
{
template<typename Allocator>
template<typename T>
inline ByteVector<Allocator>::ByteVector(
	typename base::size_type count,
	T const &value,
	Allocator const &alloc) noexcept
: eastl::vector<u8, Allocator>(count * sizeof(T), alloc)
{
	assign(count, value);
}

template<typename Allocator>
template<typename T>
inline ByteVector<Allocator>::ByteVector(
	eastl::vector<T> const &vec,
	Allocator const &alloc)
: eastl::vector<u8, Allocator>(vec.size() * sizeof(T), alloc)
{
	return push_back(vec);
}

template<typename Allocator>
template<typename T, size_t N>
inline ByteVector<Allocator>::ByteVector(
	eastl::array<T, N> const &arr,
	Allocator const &alloc)
: eastl::vector<u8, Allocator>(arr.size() * sizeof(T), alloc)
{
	return push_back(arr);
}

template<typename Allocator>
template<typename T>
inline void ByteVector<Allocator>::assign(typename base::size_type count, T const &value)
{
	static_assert(eastl::is_fundamental<T>::value, "T must be a fundamental type.");

	resize(count * sizeof(T));
	for(typename base::size_type i = 0; i < count * sizeof(T); i += sizeof(T))
	{
		eastl::copy(
			reinterpret_cast<u8*>(&value),
			reinterpret_cast<u8*>(&value) + sizeof(T),
			eastl::advance(base::begin(), i));
	}
}

template<typename Allocator>
template<typename T>
inline typename ByteVector<Allocator>::base::iterator
ByteVector<Allocator>::insert(typename base::const_iterator pos, T const &value)
{
	static_assert(eastl::is_fundamental<T>::value, "T must be a fundemental type.");

	base::reserve(base::size() + sizeof(T));

	auto it = base::insert(pos, sizeof(T), u8());
	eastl::copy(reinterpret_cast<u8*>(&value), reinterpret_cast<u8*>(&value) + sizeof(T), it);

	return it;
}

template<typename Allocator>
template<typename T>
inline void ByteVector<Allocator>::push_back(T const &value)
{
	static_assert(eastl::is_fundamental<T>::value, "T must be a fundemental type.");

	base::resize(base::size() + sizeof(T));

	eastl::copy(
		reinterpret_cast<u8*>(&value),
		reinterpret_cast<u8*>(&value) + sizeof(T),
		eastl::prev(base::end(), sizeof(T)));
}

template<typename Allocator>
template<typename T>
inline void ByteVector<Allocator>::push_back(eastl::vector<T> const &vec)
{
	static_assert(eastl::is_fundamental<T>::value, "T must be a fundemental type.");

	typename base::size_type begin = base::size();

	base::resize(base::size() + vec.size() * sizeof(T));

	eastl::copy(
		reinterpret_cast<u8*>(vec.data()),
		reinterpret_cast<u8*>(vec.data()) + vec.size(),
		base::data() + begin);
}

template<typename Allocator>
template<typename T, size_t N>
inline void ByteVector<Allocator>::push_back(eastl::array<T, N> const &arr)
{
	static_assert(eastl::is_fundamental<T>::value, "T must be a fundemental type.");

	typename base::size_type begin = base::size();

	base::resize(base::size() + arr.size() * sizeof(T));

	eastl::copy(
		reinterpret_cast<u8*>(arr.data()),
		reinterpret_cast<u8*>(arr.data()) + arr.size(),
		base::data() + begin);
}

template<typename Allocator>
inline void ByteVector<Allocator>::swap(ByteVector<Allocator> &other)
{
	base::swap(other);
}
} // namespace Containers
} // namespace Common

namespace eastl
{
template<typename Allocator>
void swap(
	Common::Containers::ByteVector<Allocator> &lhs,
	Common::Containers::ByteVector<Allocator> &rhs)
{
	lhs.swap(rhs);
}
} // namespace eastl

#endif // ed5753eb_2899_4830_b609_33aeb73910e9
