#include "window.hpp"

#include <exception>
#include <mutex>

#include <glbinding/gl32core/boolean.h>
#include <glbinding/gl32core/enum.h>
#include <glbinding/gl32core/functions.h>
#include <glbinding/gl32core/values.h>

#include "../config.hpp"
#include "../engine.hpp"
#include "window_manager.hpp"

namespace Common
{
namespace Windowing
{
bool Window::FrameBufferConfiguration::operator==(Window::FrameBufferConfiguration &other)
	noexcept
{
	return (r == other.r
			&& g == other.g
			&& b == other.b
			&& a == other.a
			&& depth == other.depth
			&& stencil == other.stencil);
}

Window::Window(WindowManager &manager) noexcept
: initialized(false)
, created(false)
, configuration()
, window(nullptr)
, windowManager(manager)
{
}

Window::~Window() noexcept
{
	if(window)
	{
		glfwDestroyWindow(window);
		window = nullptr;
	}
}

void Window::SetWindowConfiguration(Window::WindowConfiguration &&config) noexcept
{
	if(!created)
	{
		configuration = eastl::move(config);
		initialized = true;

		return;
	}

	if(configuration.windowMode != config.windowMode)
	{
		WindowMode oldWindowMode = configuration.windowMode;

		configuration.windowMode = config.windowMode;
		configuration.width = config.width;
		configuration.height = config.height;

		GLFWmonitor *monitor = glfwGetPrimaryMonitor();
		GLFWvidmode const *videoMode = glfwGetVideoMode(monitor);

		switch(configuration.windowMode)
		{
			case WindowMode::windowed:
			{
				glfwSetWindowAttrib(window, GLFW_DECORATED, GLFW_TRUE);

				if(oldWindowMode == WindowMode::fullscreen)
				{
					glfwSetWindowMonitor(
						window,
						nullptr,
						0,
						0,
						configuration.width,
						configuration.height,
						GLFW_DONT_CARE);
				}
			} break;
			case WindowMode::borderless:
			{
				glfwSetWindowAttrib(window, GLFW_DECORATED, GLFW_FALSE);
				glfwSetWindowPos(window, 0, 0);

				if(oldWindowMode == WindowMode::fullscreen)
				{
					glfwSetWindowMonitor(
						window,
						nullptr,
						0,
						0,
						configuration.width,
						configuration.height,
						GLFW_DONT_CARE);
				}
			} break;
			case WindowMode::fullscreen:
			{
				glfwSetWindowAttrib(window, GLFW_RED_BITS, videoMode->redBits);
				glfwSetWindowAttrib(window, GLFW_GREEN_BITS, videoMode->greenBits);
				glfwSetWindowAttrib(window, GLFW_BLUE_BITS, videoMode->blueBits);

				glfwSetWindowMonitor(
					window,
					monitor,
					0,
					0,
					videoMode->width,
					videoMode->height,
					videoMode->refreshRate);
			} break;
		}

		// Framebuffer configuration is read-only for now,
		// perhaps this might change in the future.
		FrameBufferConfiguration fbConfig;
		fbConfig.r = videoMode->redBits;
		fbConfig.g = videoMode->greenBits;
		fbConfig.b = videoMode->blueBits;

		gl32core::glGetFramebufferAttachmentParameteriv(
			gl32core::GL_DRAW_FRAMEBUFFER,
			gl32core::GL_BACK_LEFT,
			gl32core::GL_FRAMEBUFFER_ATTACHMENT_ALPHA_SIZE,
			&fbConfig.a);

		gl32core::glGetFramebufferAttachmentParameteriv(
			gl32core::GL_DRAW_FRAMEBUFFER,
			gl32core::GL_DEPTH,
			gl32core::GL_FRAMEBUFFER_ATTACHMENT_DEPTH_SIZE,
			&fbConfig.depth);

		gl32core::glGetFramebufferAttachmentParameteriv(
			gl32core::GL_DRAW_FRAMEBUFFER,
			gl32core::GL_STENCIL,
			gl32core::GL_FRAMEBUFFER_ATTACHMENT_STENCIL_SIZE,
			&fbConfig.stencil);

		configuration.framebufferConfig = eastl::move(fbConfig);
	}

	if(configuration.title != config.title)
	{
		configuration.title = eastl::move(config.title);
		glfwSetWindowTitle(window, configuration.title.c_str());
	}

	if(configuration.resizeMode != config.resizeMode)
	{
		if(configuration.resizeMode == ResizeMode::none)
		{
			glfwSetWindowSizeLimits(
				window,
				GLFW_DONT_CARE,
				GLFW_DONT_CARE,
				GLFW_DONT_CARE,
				GLFW_DONT_CARE);
		}

		configuration.resizeMode = config.resizeMode;

		switch(configuration.resizeMode)
		{
			case ResizeMode::free:
			{
				glfwSetWindowAspectRatio(window, GLFW_DONT_CARE, GLFW_DONT_CARE);
			} break;
			case ResizeMode::aspectRatio:
			{
				glfwSetWindowAspectRatio(window, configuration.width, configuration.height);
			} break;
			case ResizeMode::none:
			{
				glfwSetWindowSizeLimits(
					window,
					configuration.width,
					configuration.height,
					configuration.width,
					configuration.height);
			} break;
		}
	}
}

void Window::Create()
{
	if(!initialized)
	{
		throw std::runtime_error("Window configuration has not been set.");
	}

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifdef RENDERER_DEBUG_CONTEXT
	glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, static_cast<int>(gl32core::GL_TRUE));
#endif

	GLFWmonitor *monitor = glfwGetPrimaryMonitor();
	GLFWvidmode const *videoMode = glfwGetVideoMode(monitor);

	glfwWindowHint(GLFW_RED_BITS, videoMode->redBits);
	glfwWindowHint(GLFW_GREEN_BITS, videoMode->greenBits);
	glfwWindowHint(GLFW_BLUE_BITS, videoMode->blueBits);

	switch(configuration.windowMode)
	{
		case WindowMode::windowed:
		{
			window = glfwCreateWindow(
				configuration.width,
				configuration.height,
				configuration.title.c_str(),
				nullptr,
				nullptr);
		}break;
		case WindowMode::fullscreen:
		{
			glfwWindowHint(GLFW_REFRESH_RATE, videoMode->refreshRate);

			window = glfwCreateWindow(
				videoMode->width,
				videoMode->height,
				configuration.title.c_str(),
				monitor,
				nullptr);
		}break;
		case WindowMode::borderless:
		{
			glfwWindowHint(GLFW_DECORATED, GLFW_FALSE);

			window = glfwCreateWindow(
				configuration.width,
				configuration.height,
				configuration.title.c_str(),
				nullptr,
				nullptr);
		}break;
	}

	if(!window)
	{
		throw std::runtime_error(
			"Error creating window, see the error callback for more information.");
	}

	FrameBufferConfiguration fbConfig;
	fbConfig.r = videoMode->redBits;
	fbConfig.g = videoMode->greenBits;
	fbConfig.b = videoMode->blueBits;

	// GLFW default values, can't query here because glbinding isn't initialized yet.
	// The fbconfig is read-only anyway, so it doesn't really matter.
	// http://www.glfw.org/docs/latest/window_guide.html#window_hints_values
	fbConfig.a = 8;
	fbConfig.depth = 24;
	fbConfig.stencil = 8;
	configuration.framebufferConfig = eastl::move(fbConfig);

	switch(configuration.resizeMode)
	{
		case ResizeMode::aspectRatio:
		{
			glfwSetWindowAspectRatio(window, configuration.width, configuration.height);
		} break;
		case ResizeMode::free:
		break;
		case ResizeMode::none:
		{
			glfwSetWindowSizeLimits(
				window,
				configuration.width,
				configuration.height,
				configuration.width,
				configuration.height);
		} break;
	}

	glfwSetWindowCloseCallback(window, &WindowManager::WindowCloseCallback);
	glfwSetKeyCallback(window, &WindowManager::KeyCallback);
}

void Window::Destroy() noexcept
{
	windowManager.DeleteWindow(*this);
}

void Window::MakeContextCurrent() noexcept
{
	glfwMakeContextCurrent(window);
}

void Window::ClearContext() noexcept
{
	glfwMakeContextCurrent(nullptr);
}

void Window::CaptureInput(s32 key, s32 scancode, s32 action, s32 mods)
{
	inputListener.CaptureInput(key, scancode, action, mods);
}
} // namespace Windowing
} // namespace Common
