#ifndef b708dbd1_3578_46c3_bcf1_f2a1032053f5
#define b708dbd1_3578_46c3_bcf1_f2a1032053f5

#include <atomic>

#include <EASTL/string.h>

#include <GLFW/glfw3.h>

#include "../Input/input_listener.hpp"
#include "../Threading/spinlock.hpp"
#include "../types.hpp"
#include "../api.hpp"

namespace Common
{
using namespace Types;

namespace Windowing
{
class WindowManager;

class COMMON_API Window
{
public:
	enum class WindowMode: u8
	{
		windowed,
		fullscreen,
		borderless
	};

	enum class ResizeMode: u8
	{
		none,
		free,
		aspectRatio
	};

	class FrameBufferConfiguration
	{
	public:
		FrameBufferConfiguration() noexcept = default;
		FrameBufferConfiguration(FrameBufferConfiguration const &) = default;
		FrameBufferConfiguration(FrameBufferConfiguration &&) noexcept = default;
		~FrameBufferConfiguration() noexcept = default;

		FrameBufferConfiguration &operator=(FrameBufferConfiguration const &) = default;
		FrameBufferConfiguration &operator=(FrameBufferConfiguration &&) noexcept = default;

		bool operator==(FrameBufferConfiguration &other) noexcept;
	public:
		s32 r, g, b, a, depth, stencil;
	};

	class WindowConfiguration
	{
	public:
		WindowConfiguration() = default;
		WindowConfiguration(WindowConfiguration const &) = default;
		WindowConfiguration(WindowConfiguration &&) noexcept = default;
		~WindowConfiguration() noexcept = default;

		WindowConfiguration &operator=(WindowConfiguration const &) = default;
		WindowConfiguration &operator=(WindowConfiguration &&) noexcept = default;

	public:
		WindowMode windowMode;
		s32 width, height;
		eastl::string title;
		ResizeMode resizeMode;
		FrameBufferConfiguration framebufferConfig;
	};

public:
	explicit Window(WindowManager &manager) noexcept;
	Window(Window const &) = delete;
	Window(Window &&other) noexcept = delete;
	~Window() noexcept;

	Window &operator=(Window const &) = delete;
	Window &operator=(Window &&other) noexcept = delete;

	void SetWindowConfiguration(WindowConfiguration &&config) noexcept;
	inline WindowConfiguration GetWindowConfiguration() const noexcept;

	void Create();
	void Destroy() noexcept;

	void Show(bool shown) noexcept;

	void MakeContextCurrent() noexcept;
	void ClearContext() noexcept;

	void CaptureInput(s32 key, s32 scancode, s32 action, s32 mods);
	inline Input::InputListener &GetInputListener() noexcept;

	inline void SwapBuffers() noexcept;

private:
	friend class WindowManager;

	bool initialized, created;
	WindowConfiguration configuration;

	Input::InputListener inputListener;

	GLFWwindow *window;

	WindowManager &windowManager;
};
} // namespace Windowing
} // namespace Common

#include "window_impl.hpp"

#endif // b708dbd1_3578_46c3_bcf1_f2a1032053f5
