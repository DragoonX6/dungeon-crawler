#ifndef b1bbbf59_960c_4784_9b87_adcd9051552b
#define b1bbbf59_960c_4784_9b87_adcd9051552b

#include <mutex>

#include <EASTL/functional.h>
#include <EASTL/optional.h>
#include <EASTL/string.h>
#include <EASTL/unique_ptr.h>
#include <EASTL/unordered_map.h>

#include <glbinding/gl32core/types.h>
#include <GLFW/glfw3.h>

#include "../types.hpp"
#include "../api.hpp"
#include "window.hpp"

namespace Common
{
using namespace Common::Types;

namespace Windowing
{
class COMMON_API WindowManager
{
public:
	WindowManager() = default;
	WindowManager(WindowManager const &) = delete;
	WindowManager(WindowManager &&) = delete;
	~WindowManager() noexcept = default;

	WindowManager &operator=(WindowManager const &) = delete;
	WindowManager &operator=(WindowManager &&) = delete;

	Window &AddWindow(eastl::string const &name);
	void DeleteWindow(Window &window) noexcept;

	eastl::optional<eastl::reference_wrapper<Window>>
		FindWindow(eastl::string const &name) noexcept;

	static void KeyCallback(
		GLFWwindow* window,
		int key,
		int scancode,
		int action,
		int mods) noexcept;

	static void ErrorCallback(int error, char const *description) noexcept;

	static void WindowCloseCallback(GLFWwindow *window) noexcept;

	static void APIENTRY DebugCallback(
		gl32core::GLenum source,
		gl32core::GLenum type,
		gl32core::GLuint id,
		gl32core::GLenum severity,
		gl32core::GLsizei length,
		const gl32core::GLchar* message,
		const void* userParam) noexcept;

	void Run() noexcept;

private:
	friend class Window;
	eastl::optional<eastl::reference_wrapper<Window>>
		GetWindow(GLFWwindow* window) noexcept;

private:
	std::mutex lock;
	eastl::unordered_map<eastl::string, eastl::unique_ptr<Window>> windows;

	eastl::unique_ptr<Window> *lastWindow;
};
}
}

#endif // b1bbbf59_960c_4784_9b87_adcd9051552b
