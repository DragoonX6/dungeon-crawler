#ifndef f077f31d_6c0d_47a5_a646_349b6d2d59f6
#define f077f31d_6c0d_47a5_a646_349b6d2d59f6

namespace Common
{
namespace Windowing
{
inline Window::WindowConfiguration Window::GetWindowConfiguration() const noexcept
{
	return configuration;
}

inline Common::Input::InputListener &Window::GetInputListener() noexcept
{
	return inputListener;
}

inline void Window::SwapBuffers() noexcept
{
	glfwSwapBuffers(window);
}
} // namespace Windowing
} // namespace Common

#endif // f077f31d_6c0d_47a5_a646_349b6d2d59f6
