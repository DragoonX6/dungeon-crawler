#include "window_manager.hpp"

#include <iostream>
#include <stdexcept>
#include <EASTL/string.h>

#include <glbinding/gl32ext/enum.h>

#include "../engine.hpp"

namespace Common
{
namespace Windowing
{
Window &WindowManager::AddWindow(const eastl::string &name)
{
	std::lock_guard<std::mutex> localLock(lock);
	(void)localLock;

	const auto it = windows.find(name);
	if(it != windows.end())
	{
		eastl::string errorString = eastl::string("Error: Window with name: <") + name +
			"> already exists";

		throw std::runtime_error(errorString.c_str());
	}

	windows.emplace(eastl::make_pair(name, eastl::make_unique<Window>(*this)));
	lastWindow = &windows[name];

	return *lastWindow->get();
}

void WindowManager::DeleteWindow(Window &window) noexcept
{
	std::lock_guard<std::mutex> localLock(lock);
	(void)localLock;

	for(auto it = windows.begin(); it != windows.end(); ++it)
	{
		if(it->second.get() == &window)
		{
			windows.erase(it);
			lastWindow = nullptr;
			return;
		}
	}
}

eastl::optional<eastl::reference_wrapper<Window>>
WindowManager::FindWindow(const eastl::string &name) noexcept
{
	const auto it = windows.find(name);
	if(it != windows.end())
	{
		return eastl::ref(*it->second);
	}

	return eastl::nullopt;
}

void WindowManager::KeyCallback(
	GLFWwindow *window,
	int key,
	int scancode,
	int action,
	int mods) noexcept
{
	auto currentWindow = Engine::GetWindowManager().GetWindow(window);
	if(currentWindow)
		currentWindow->get().CaptureInput(key, scancode, action, mods);
}

void WindowManager::ErrorCallback(int error, const char *description) noexcept
{
	switch(error)
	{
		default:
		{
			std::cerr << "Unknown GLFW error:" << description << std::endl;
		}
	}
}

void WindowManager::WindowCloseCallback(GLFWwindow *window) noexcept
{
	auto currentWindow = Engine::GetWindowManager().GetWindow(window);
	if(currentWindow)
	{
		if(Engine::GetWindowManager().windows.size() == 1)
			Engine::GetStateQueue().Exit();

		currentWindow->get().Destroy();
	}
}

static eastl::string FormatDebugOutput(
	gl32core::GLenum source,
	gl32core::GLenum type,
	gl32core::GLuint id,
	gl32core::GLenum severity,
	const char* msg) noexcept
{
	eastl::string errorString;
	eastl::string sourceString;
	eastl::string typeString;
	eastl::string severityString;

	switch (source)
	{
		case gl32ext::GL_DEBUG_SOURCE_API:
		{
			sourceString = "API";
		}break;
		case gl32ext::GL_DEBUG_SOURCE_APPLICATION:
		{
			sourceString = "Application";
		}break;
		case gl32ext::GL_DEBUG_SOURCE_WINDOW_SYSTEM:
		{
			sourceString = "Window System";
		}break;
		case gl32ext::GL_DEBUG_SOURCE_SHADER_COMPILER:
		{
			sourceString = "Shader Compiler";
		}break;
		case gl32ext::GL_DEBUG_SOURCE_THIRD_PARTY:
		{
			sourceString = "Third Party";
		}break;
		case gl32ext::GL_DEBUG_SOURCE_OTHER:
		{
			sourceString = "Other";
		}break;
		default:
		{
			sourceString = "Unknown";
		}
	}

	switch (type)
	{
		case gl32ext::GL_DEBUG_TYPE_ERROR:
		{
			typeString = "Error";
		}break;
		case gl32ext::GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
		{
			typeString = "Deprecated Behavior";
		}break;
		case gl32ext::GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
		{
			typeString = "Undefined Behavior";
		}break;
		case gl32ext::GL_DEBUG_TYPE_PORTABILITY:
		{
			typeString = "Portability";
		}break;
		case gl32ext::GL_DEBUG_TYPE_PERFORMANCE:
		{
			typeString = "Performance";
		}break;
		case gl32ext::GL_DEBUG_TYPE_OTHER:
		{
			typeString = "Other";
		}break;
		default:
		{
			typeString = "Unknown";
		}
	}

	switch (severity)
	{
		case gl32ext::GL_DEBUG_SEVERITY_HIGH:
		{
			severityString = "High";
		}break;
		case gl32ext::GL_DEBUG_SEVERITY_MEDIUM:
		{
			severityString = "Medium";
		}break;
		case gl32ext::GL_DEBUG_SEVERITY_LOW:
		{
			severityString = "Low";
		}break;
		case gl32ext::GL_DEBUG_SEVERITY_NOTIFICATION:
		{
			severityString = "Notification";
		}break;
		default:
		{
			severityString = "Unknown";
		}
	}

	errorString = eastl::string("OpenGL Error: ") + msg +
		eastl::string(" [Source = ") + sourceString +
		eastl::string(", Type = ") + typeString +
		eastl::string(", Severity = ") + severityString +
		eastl::string(", ID = ") + eastl::to_string(id) + "]";

	return errorString;
}

void WindowManager::DebugCallback(
	gl32core::GLenum source,
	gl32core::GLenum type,
	gl32core::GLuint id,
	gl32core::GLenum severity,
	gl32core::GLsizei length,
	const gl32core::GLchar *message,
	const void *userParam) noexcept
{
	(void)length;
	(void)userParam;
	eastl::string error = FormatDebugOutput(source, type, id, severity, message);
	std::cerr << error.c_str() << std::endl;
}

void WindowManager::Run() noexcept
{
	while(true)
	{
		glfwWaitEvents();

		{
			std::lock_guard<std::mutex> localLock(lock);

			if(!windows.size())
				break;
		}
	}
}

eastl::optional<eastl::reference_wrapper<Window>>
WindowManager::GetWindow(GLFWwindow *window) noexcept
{
	std::lock_guard<std::mutex> localLock(lock);
	(void)localLock;

	if(!windows.size() && lastWindow)
	{
		if((*lastWindow)->window == window)
			return eastl::ref(*(*lastWindow));
	}

	for(auto it = windows.begin(), end = windows.end(); it != end; ++it)
	{
		if(it->second->window == window)
		{
			lastWindow = &it->second;
			return eastl::ref(*it->second);
		}
	}

	return {};
}
} // namespace Windowing
} // namespace Common
