#ifndef cf25c547_018d_49c7_8337_e680ffda9a83
#define cf25c547_018d_49c7_8337_e680ffda9a83

#include <EASTL/unique_ptr.h>
#include <EASTL/string.h>
#include <EASTL/vector.h>

#include "../api.hpp"
#include "file.hpp"

namespace Common
{
namespace FileSystems
{
class COMMON_API FileSystem
{
static_assert(eastl::is_nothrow_move_constructible<
		eastl::vector<eastl::unique_ptr<File>>>::value,
		"Vectors should be nothrow move constructible");

static_assert(eastl::is_nothrow_move_assignable<
		eastl::vector<eastl::unique_ptr<File>>>::value,
		"Vectors should be nothrow move assignable");

static_assert(eastl::is_nothrow_move_constructible<eastl::string>::value,
		"Strings should be nothrow move constructible");

static_assert(eastl::is_nothrow_move_assignable<eastl::string>::value,
		"Strings should be nothrow move assignable");

public:
	FileSystem() noexcept = default;
	FileSystem(FileSystem const &other) = delete;
	FileSystem(FileSystem &&other) noexcept = default;
	virtual ~FileSystem() noexcept;

	FileSystem &operator=(FileSystem const &other) = delete;
	FileSystem &operator=(FileSystem &&other) noexcept = default;

	// Checks if a file exists at workingDirectory + path
	virtual bool FileExists(eastl::string const &fileName) = 0;

	// Loads a file at workingDirectory + path
	virtual eastl::unique_ptr<File> LoadFile(eastl::string const &fileName,
						File::OpenMode openMode =
						File::OpenMode::read | File::OpenMode::shared) = 0;

	inline void SetWorkingDirectory(eastl::string const &directory) noexcept;
	inline void SetWorkingDirectory(eastl::string &&directory) noexcept;

	inline eastl::string GetWorkingDirectory() const noexcept;

protected:
	eastl::string workingDirectory;
};
} // namespace FileSystems
} // namespace Common

#include "file_system_impl.hpp"

#endif // cf25c547_018d_49c7_8337_e680ffda9a83
