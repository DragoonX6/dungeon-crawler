#ifndef a415ce22_9bde_44d9_b9db_c7064e93b1ec
#define a415ce22_9bde_44d9_b9db_c7064e93b1ec

#include <EASTL/algorithm.h>

namespace Common
{
namespace FileSystems
{
inline void File::Skip(s64 offset)
{
	Seek(offset, SeekMode::current);
}

inline eastl::string const &File::Name() const
{
	return fileName;
}

inline s64 File::Position() const noexcept
{
	return position;
}

inline s64 File::Size()
{
	return size;
}

template<typename T> inline T File::Read()
{
	static_assert(eastl::is_fundamental<T>::value, "T must be a primitive type.");

	T ret;
	Read(&ret);
	return ret;
}

template<typename T> inline void File::Read(T &value)
{
	static_assert(eastl::is_fundamental<T>::value, "T must be a primitive type.");

	eastl::vector<u8> temp(sizeof(T), 0);
	ReadData(temp, sizeof(T));
	value = *reinterpret_cast<T*>(&temp[0]);
}

template<typename T> inline void File::Write(T const &value)
{
	static_assert(eastl::is_fundamental<T>::value, "T must be a primitive type.");

	eastl::vector<u8> temp(sizeof(T), 0);
	eastl::copy(&value, &value, temp.begin());
}

inline File::OpenMode operator|(File::OpenMode a, File::OpenMode b) noexcept
{
	return static_cast<File::OpenMode>(static_cast<u8>(a) | static_cast<u8>(b));
}

inline File::OpenMode operator&(File::OpenMode a, File::OpenMode b) noexcept
{
	return static_cast<File::OpenMode>(static_cast<u8>(a) & static_cast<u8>(b));
}

inline u8 operator+(File::OpenMode a) noexcept
{
	return static_cast<u8>(a);
}

inline eastl::string OpenModeToString(File::OpenMode openMode)
{
	if(openMode == File::OpenMode::none)
		return "none";

	eastl::string modeString;

	if(+(openMode & File::OpenMode::read))
		modeString += "read ";

	if(+(openMode & File::OpenMode::write))
		modeString += "write ";

	if(+(openMode & File::OpenMode::shared))
		modeString += "shared ";

	modeString.pop_back();

	return modeString;
}
} // namespace FileSystems
} // namespace Common

#endif // a415ce22_9bde_44d9_b9db_c7064e93b1ec
