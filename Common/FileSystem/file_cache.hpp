#ifndef abe18085_b1f0_4cf2_a8ec_455a188d967c
#define abe18085_b1f0_4cf2_a8ec_455a188d967c

#include <EASTL/functional.h>
#include <EASTL/optional.h>
#include <EASTL/string.h>
#include <EASTL/unique_ptr.h>
#include <EASTL/vector.h>

#include "../api.hpp"
#include "file.hpp"

namespace Common
{
namespace FileSystems
{
class COMMON_API FileCache final
{
public:
	FileCache() noexcept = default;
	FileCache(FileCache const &) = delete;
	FileCache(FileCache &&other) noexcept = default;
	~FileCache() noexcept = default;

	FileCache &operator=(FileCache const&) = delete;
	FileCache &operator=(FileCache &&other) noexcept = default;

	File &Add(eastl::unique_ptr<File> &&file);
	void Remove(File &&file) noexcept;

	eastl::optional<eastl::reference_wrapper<File>>
		Get(eastl::string const &fileName) noexcept;

private:
	eastl::vector<eastl::unique_ptr<File>> fileCache;
};
} // namesapce FileSystems
} // namespace Common

#endif // abe18085_b1f0_4cf2_a8ec_455a188d967c
