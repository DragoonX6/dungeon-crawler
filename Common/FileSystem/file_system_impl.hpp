#ifndef f24f2fcd_08d9_40e8_bc63_2f333e403387
#define f24f2fcd_08d9_40e8_bc63_2f333e403387

namespace Common
{
namespace FileSystems
{
inline void FileSystem::SetWorkingDirectory(eastl::string const &directory) noexcept
{
	workingDirectory = directory;
	if (workingDirectory[workingDirectory.size() - 1] != '/' ||
			workingDirectory[workingDirectory.size() - 1] != '\\')
	{
		workingDirectory += '/';
	}
}

inline void FileSystem::SetWorkingDirectory(eastl::string &&directory) noexcept
{
	workingDirectory = eastl::move(directory);
	if (workingDirectory[workingDirectory.size() - 1] != '/' ||
			workingDirectory[workingDirectory.size() - 1] != '\\')
	{
		workingDirectory += '/';
	}
}

inline eastl::string FileSystem::GetWorkingDirectory() const noexcept
{
	return workingDirectory;
}
} // namespace FileSystems
} // namespace Common

#endif // f24f2fcd_08d9_40e8_bc63_2f333e403387
