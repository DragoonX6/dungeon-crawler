#ifndef c751ea6f_3fa0_4196_8939_4579e667015a
#define c751ea6f_3fa0_4196_8939_4579e667015a

#include <EASTL/unique_ptr.h>
#include <EASTL/string.h>
#include <EASTL/vector.h>
#include <EASTL/type_traits.h>

#include "../api.hpp"
#include "../types.hpp"

namespace Common
{
using namespace Types;

namespace FileSystems
{
class COMMON_API File
{
public:
	enum class OpenMode: u8
	{
		none = 0,
		read = 1 << 0,
		write = 1 << 1,
		shared = 1 << 2
	};

	enum class SeekMode: u8
	{
		begin,
		current,
		end
	};

public:
	File() noexcept;
	File(File const &other) = delete;
	File(File &&other) noexcept;
	virtual ~File() noexcept;

	File &operator=(File const &other) = delete;
	File &operator=(File &&other) noexcept;

	virtual void Create(eastl::string const &fileName) = 0;
	virtual void Open(
		eastl::string const &fileName,
		OpenMode openMode =
		static_cast<OpenMode>(
			static_cast<u8>(OpenMode::read) |
			static_cast<u8>(OpenMode::shared))) = 0;
	virtual void Close() noexcept = 0;

	virtual void Seek(s64 offset, SeekMode seekMode = SeekMode::begin) = 0;
	inline void Skip(s64 offset);

	inline eastl::string const &Name() const;

	inline s64 Position() const noexcept;
	inline s64 Size();

	template<typename T>
	inline T Read();
	template<typename T>
	inline void Read(T &value);
	eastl::string ReadString();
	virtual void ReadData(eastl::vector<u8> &dest, s64 length) = 0;

	template<typename T>
	inline void Write(T const &value);
	void WriteString(eastl::string const &value);
	virtual void WriteData(eastl::vector<u8> const &source, s64 length) = 0;

protected:
	eastl::string fileName;
	bool isOpen;
	OpenMode openMode;
	s64 position;
	s64 size;
};

inline File::OpenMode operator|(File::OpenMode a, File::OpenMode b) noexcept;

inline File::OpenMode operator&(File::OpenMode a, File::OpenMode b) noexcept;

inline u8 operator+(File::OpenMode a) noexcept;

inline eastl::string OpenModeToString(File::OpenMode openMode);
} // namespace FileSystems
} // namespace Common

#include "file_impl.hpp"

#endif // c751ea6f_3fa0_4196_8939_4579e667015a
