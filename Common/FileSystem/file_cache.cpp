#include "file_cache.hpp"

#include <EASTL/algorithm.h>

namespace Common
{
namespace FileSystems
{
File &FileCache::Add(eastl::unique_ptr<File> &&file)
{
	auto maybeFile = Get(file->Name());

	if(maybeFile)
		return maybeFile.value().get();
	else
	{
		fileCache.push_back(eastl::move(file));
		return *fileCache.back();
	}
}

void FileCache::Remove(File &&file) noexcept
{
	auto it = eastl::find_if(fileCache.begin(), fileCache.end(),
							 [&file](eastl::unique_ptr<File> const &cur)
							 {
								 return cur.get() == &file;
							 });

	if(it != fileCache.end())
	{
		auto last = fileCache.end() - 1;
		eastl::iter_swap(it, last);
		fileCache.pop_back();
	}
}

eastl::optional<eastl::reference_wrapper<File>>
FileCache::Get(eastl::string const &fileName) noexcept
{
	auto it = eastl::find_if(fileCache.begin(), fileCache.end(),
							 [&fileName](eastl::unique_ptr<File> const &cur)
							 {
								 return cur->Name() == fileName;
							 });

	if(it != fileCache.end())
		return eastl::ref(*(*it));
	else
		return eastl::nullopt;
}
} // namespace FileSystems
} // namesapce Common
