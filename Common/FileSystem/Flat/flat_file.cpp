#include "flat_file.hpp"

#include <cerrno>
#include <cstdio>
#include <cstring>
#include <iostream>

#include <EABase/eabase.h>
#include <EASTL/numeric_limits.h>

#if defined(EA_PLATFORM_MICROSOFT) && defined(UNICODE)
#include <utf8.h>
#endif

#include "../../Exceptions/exceptions.hpp"
#include "../../Exceptions/file_exceptions.hpp"

#ifdef EA_COMPILER_MSVC
#define fseeko64 _fseeki64
#define ftello64 _ftelli64
#endif

namespace Common
{
namespace FileSystems
{
FlatFile::FlatFile()
: file(nullptr)
{
}

FlatFile::FlatFile(FlatFile &&other) noexcept
: File(eastl::move(other))
, file(other.file)
{
	other.file = nullptr;
}

FlatFile::~FlatFile() noexcept
{
	Close();
}

FlatFile &FlatFile::operator=(FlatFile &&other) noexcept
{
	File::operator=(eastl::move(other));
	file = other.file;
	other.file = nullptr;

	return *this;
}

void FlatFile::Create(const eastl::string &fileName)
{
	Open(fileName, File::OpenMode::write);
	Close();
}

void FlatFile::Open(eastl::string const &fileName, File::OpenMode openMode)
{
	if((+(openMode & File::OpenMode::write) && +(openMode & File::OpenMode::shared)))
	{
		throw Exceptions::InvalidOpenMode(eastl::string("Can't open ") + fileName +
										  " for shared writing.");
	}

	eastl::string mode;
	if(+(openMode & File::OpenMode::read))
	{
		mode += "r";
		if(!+(openMode & File::OpenMode::shared))
		{
			std::cout << "Warning: Flatfile::OpenMode::read is shared by default,"
				<< std::endl << "open with write mode if you want exclusive access"
				<< "to the file." << std::endl;
		}

		if(+(openMode & File::OpenMode::write))
		{
			mode += "+";
		}
	}

	if(+(openMode & File::OpenMode::write))
	{
		mode += "w";
	}

	mode += "b";

#if defined(EA_PLATFORM_MICROSOFT) && defined(UNICODE)
	eastl::wstring fileName16, mode16;
	utf8::utf8to16(fileName.cbegin(), fileName.cend(), eastl::back_inserter(fileName16));
	utf8::utf8to16(mode.cbegin(), mode.cend(), eastl::back_inserter(mode16));
	file = _wfopen(fileName16.c_str(), mode16.c_str());
#else
	file = ::fopen64(fileName.c_str(), mode.c_str());
#endif

	if(file == nullptr)
	{
		char* cError = strerror(errno);
		eastl::string error(cError);

		if(errno == EACCES)
			throw Exceptions::AccessDenied(
				"Access to <" + fileName + "> is denied: " + error);
		else if(errno == EISDIR)
			throw Exceptions::FileIsDirectory(
				"File <" + fileName + "> is a directory: " + error);
		else if(errno == ELOOP)
			throw Exceptions::SymlinkLoop(
				"Symlink loop detected when accessing <" + fileName + ">: " + error);
		else if(errno == EMFILE)
			throw Exceptions::TooManyFileDescriptors(
				"No available file descriptor when trying to open <"
				+ fileName
				+ ">: "
				+ error);
		else if(errno == ENAMETOOLONG)
			throw Exceptions::FileNameTooLong(
				"The file name <" + fileName + "> is too long: " + error);
		else if(errno == ENFILE)
			throw Exceptions::TooManyOpenFiles(
				"The OS's maximum amount of open files has been reached while trying to "
				"open <"
				+ fileName
				+ ">: "
				+ error);
		else if(errno == ENOENT)
			throw Exceptions::FileNotFound(
				"The file <" + fileName + "> was not found: " + error);
		else if(errno == ENOSPC)
			throw Exceptions::OutOfSpace(
				"Unable to create the new file <"
				+ fileName
				+ "> because the directory or file system cannot be expanded: "
				+ error);
		else if(errno == ENOTDIR)
			throw Exceptions::InvalidPath(
				"The path to <" + fileName + "> is invalid: " + error);
		else if(errno == EOVERFLOW)
			throw Exceptions::FileTooBig(
				"The file <" + fileName + "> is too big to be opened: " + error);
		else if(errno == EINVAL)
			throw Exceptions::InvalidOpenMode(
				"The open mode <"
				+ OpenModeToString(openMode)
				+ "> for file <"
				+ fileName
				+ "> is invalid: "
				+ error);
		else if(errno == ENOMEM)
			throw Exceptions::OutOfSpace(
				"Unable to create the new file <"
				+ fileName
				+ "> because the file system is out of space: "
				+ error);
		else
			throw Exceptions::FileException(
				"Unhandled error for file <" + fileName + ">: " + error);
	}

	isOpen = true;

	Seek(0, SeekMode::end);
	size = position;
	Seek(0, SeekMode::begin);
}

void FlatFile::Close() noexcept
{
	if(isOpen)
	{
		fclose(file);
		isOpen = false;
	}
}

void FlatFile::Seek(s64 offset, File::SeekMode seekMode)
{
	s64 oldPos = position;

	switch(seekMode)
	{
		case File::SeekMode::begin:
		{
			if(offset < 0)
				throw Common::Exceptions::InvalidArgument(
					"Cannot seek to before the start of the file.");

			position = offset;
			fseeko64(file, offset, SEEK_SET);
		}break;
		case File::SeekMode::current:
		{
			if((position + offset) < 0)
				throw Common::Exceptions::InvalidArgument(
					"Cannot seek to before the start of the file.");

			position += offset;
			fseeko64(file, offset, SEEK_CUR);
		}break;
		case File::SeekMode::end:
		{
			if((size - offset) < 0)
				throw Common::Exceptions::InvalidArgument(
					"Cannot seek to before the start of the file.");

			fseeko64(file, offset, SEEK_END);
			position = ftello64(file);
		}break;
	}

	if(position > size)
	{
		Seek(oldPos, SeekMode::current);
		throw Exceptions::EndOfFile("Sparse files are not supported.");
	}
}

void FlatFile::ReadData(eastl::vector<u8> &dest, s64 length)
{
	u64 position = 0;
	s64 readLength = length;

	if(dest.capacity() < static_cast<size_t>(length))
		dest.resize(static_cast<size_t>(length));

	size_t bytesRead = 0;

	while(readLength > 0)
	{
		// 32 bit compatibility, as size_t might be 32 bits in size
		// Will we ever read 4GB+ files at once though?
		size_t maxReadLength = 0;
		if(sizeof(size_t) < sizeof(s64))
		{
			if(s64(eastl::numeric_limits<size_t>::max()) < readLength)
				maxReadLength = eastl::numeric_limits<size_t>::max();
			else
				maxReadLength = static_cast<size_t>(readLength);
		}
		else
			maxReadLength = static_cast<size_t>(readLength);

		bytesRead = fread(&dest[position], 1, static_cast<size_t>(maxReadLength), file);

		if(!bytesRead)
		{
			if(feof(file))
			{
				clearerr(file);
				throw Exceptions::EndOfFile(
					eastl::string("FlatFile::ReadData is trying to read over"
								  "the end of file <") + fileName + ">.");
			}
			else if(ferror(file))
			{
				clearerr(file);
				throw Exceptions::IOError(eastl::string("Error reading file <") + fileName +
										  ">.");
			}
		}

		position += bytesRead;
		readLength -= bytesRead;
	}

	this->position += length;
}

void FlatFile::WriteData(const eastl::vector<u8> &source, s64 length)
{
	u64 position = 0;
	s64 writeLength = length;

	size_t bytesWritten = 0;

	while(writeLength > 0)
	{
		// 32 bit compatibility, as size_t might be 32 bits in size
		// Will we ever write 4GB+ files at once though?
		size_t maxWriteLength = 0;
		if(sizeof(size_t) < sizeof(s64))
		{
			if(s64(eastl::numeric_limits<size_t>::max()) < writeLength)
				maxWriteLength = eastl::numeric_limits<size_t>::max();
			else
				maxWriteLength = static_cast<size_t>(writeLength);
		}
		else
			maxWriteLength = static_cast<size_t>(writeLength);

		bytesWritten = fwrite(&source[position], 1,
							  static_cast<size_t>(maxWriteLength), file);

		if(!bytesWritten)
		{
			if(ferror(file))
			{
				clearerr(file);
				throw Exceptions::IOError(eastl::string("Error writing to file <") +
										  fileName + ">.");
			}
		}

		position += bytesWritten;
		writeLength -= bytesWritten;
	}

	this->position += length;
	if(this->position > size)
		this->size = this->position;
}
} // namespace FileSystems
} // namespace Common
