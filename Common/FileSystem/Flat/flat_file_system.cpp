#include "flat_file_system.hpp"

#include <EASTL/algorithm.h>
#include <sys/stat.h>

namespace Common
{
namespace FileSystems
{
FlatFileSystem::~FlatFileSystem() noexcept
{
}

bool FlatFileSystem::FileExists(const eastl::string &fileName)
{
	struct stat st;
	return (stat((workingDirectory + fileName).c_str(), &st) == 0);
}

eastl::unique_ptr<File> FlatFileSystem::LoadFile(
	const eastl::string &fileName,
	File::OpenMode openMode)
{
	eastl::unique_ptr<FlatFile> file = eastl::make_unique<FlatFile>();
	file->Open(workingDirectory + fileName, openMode);

	return file;
}
} // namespace FileSystems
} // namespace Common
