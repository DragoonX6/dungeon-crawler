#ifndef bcdb2996_953b_4846_a0f1_07440b167bcc
#define bcdb2996_953b_4846_a0f1_07440b167bcc

#include <EASTL/string.h>
#include <EASTL/unique_ptr.h>

#include "../../api.hpp"
#include "../../types.hpp"
#include "../file.hpp"

namespace Common
{
using namespace Types;

namespace FileSystems
{
class COMMON_API FlatFile final: public File
{
public:
	FlatFile();
	FlatFile(const FlatFile &other) = delete;
	FlatFile(FlatFile &&other) noexcept;
	~FlatFile() noexcept override;

	FlatFile &operator=(const FlatFile &other) = delete;
	FlatFile &operator=(FlatFile &&other) noexcept;

	void Create(eastl::string const &fileName) override;
	void Open(
		eastl::string const &fileName,
		OpenMode openMode = OpenMode::read | OpenMode::shared) override;
	void Close() noexcept override;

	void Seek(s64 offset, SeekMode seekMode = SeekMode::begin) override;

	void ReadData(eastl::vector<u8> &dest, s64 length) override;
	void WriteData(eastl::vector<u8> const &source, s64 length) override;

private:
	FILE* file;
};
} // namespace FileSystems
} // namespace Common

#endif // bcdb2996_953b_4846_a0f1_07440b167bcc
