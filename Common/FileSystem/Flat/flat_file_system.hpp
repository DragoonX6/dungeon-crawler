#ifndef b74ff682_5439_4643_9820_5c6b67de079b
#define b74ff682_5439_4643_9820_5c6b67de079b

#include <EASTL/string.h>

#include "../../api.hpp"
#include "../file_system.hpp"
#include "flat_file.hpp"

namespace Common
{
namespace FileSystems
{
class COMMON_API FlatFileSystem final: public FileSystem
{
public:
	FlatFileSystem() = default;
	FlatFileSystem(FlatFileSystem const& other) = delete;
	FlatFileSystem(FlatFileSystem &&other) noexcept = default;
	~FlatFileSystem() noexcept override;

	FlatFileSystem &operator=(FlatFileSystem const &other) = delete;
	FlatFileSystem &operator=(FlatFileSystem &&other) noexcept = default;

	bool FileExists(eastl::string const &fileName) override;

	eastl::unique_ptr<File> LoadFile(eastl::string const &fileName,
				   File::OpenMode openMode =
			File::OpenMode::read | File::OpenMode::shared) override;
};
} // namespace FileSystems
} // namespace Common

#endif // b74ff682_5439_4643_9820_5c6b67de079b
