#include "file_system.hpp"

namespace Common
{
namespace FileSystems
{
FileSystem::~FileSystem() noexcept = default;
} // namespace FileSystems
} // namespace Common
