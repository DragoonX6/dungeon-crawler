#include "file.hpp"

namespace Common
{
namespace FileSystems
{
File::File() noexcept
: fileName()
, isOpen(false)
, openMode(OpenMode::read|OpenMode::shared)
, position(0)
, size(0)
{
}

File::File(File &&other) noexcept
: fileName(eastl::move(other.fileName))
, isOpen(other.isOpen)
, openMode(other.openMode)
, position(other.position)
, size(other.size)
{
	other.isOpen = false;
	other.openMode = OpenMode::none;

	other.position = other.size = 0;
}

File::~File() noexcept
{
}

File &File::operator=(File &&other) noexcept
{
	fileName = eastl::move(other.fileName);

	isOpen = other.isOpen;
	other.isOpen = false;
	openMode = other.openMode;
	other.openMode = OpenMode::none;

	position = other.position;
	size = other.size;

	other.position = other.size = 0;

	return *this;
}

eastl::string File::ReadString()
{
	u16 stringLength;
	eastl::vector<u8> temp(sizeof(u16), 0);
	ReadData(temp, sizeof(u16));
	stringLength = *reinterpret_cast<u16*>(&temp[0]);

	temp.resize(stringLength);
	ReadData(temp, stringLength);
	eastl::string ret(eastl::string::CtorDoNotInitialize(), stringLength);
	eastl::copy(temp.cbegin(), temp.cend(), eastl::back_inserter(ret));

	return ret;
}

void File::WriteString(eastl::string const &value)
{
	eastl::vector<u8> temp(value.begin(), value.end());
	WriteData(temp, static_cast<s64>(temp.size()));
}
} // namespace FileSystems
} // namespace Common
