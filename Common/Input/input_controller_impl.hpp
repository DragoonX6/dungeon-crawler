#ifndef efc87351_063a_46c6_89c2_e30e682e5583
#define efc87351_063a_46c6_89c2_e30e682e5583

namespace Common
{
namespace Input
{
template<typename T, void (T::*function)() noexcept>
inline void InputController::BindAction(
	eastl::string const &actionName,
	InputController::ButtonState state,
	T *const object) noexcept
{
	auto key = InputBindings::keyBindings.find(actionName);
	if(key != InputBindings::keyBindings.end())
	{
		actionBindings[key->second][+state].connect<T, function>(object);
	}

}

template<typename T, void (T::*function)() noexcept>
inline void InputController::UnBindActionFunction(
	eastl::string const &actionName,
	InputController::ButtonState state,
	T *const object) noexcept
{
	auto key = InputBindings::keyBindings.find(actionName);
	if(key != InputBindings::keyBindings.end())
	{
		actionBindings[key->second][+state].disconnect<T, function>(object);
	}
}

template<typename T, void (T::*function)() noexcept>
inline void InputController::BindAxis(
	eastl::string const &axisName,
	T *const object) noexcept
{
	auto key = InputBindings::keyBindings.find(axisName);
	if(key != InputBindings::keyBindings.end())
	{

		axisBindings[key->second].connect<T, function>(object);
	}
}

template<typename T, void (T::*function)() noexcept>
inline void InputController::UnBindAxisFunction(
	eastl::string const &axisName,
	T *const object) noexcept
{
	auto key = InputBindings::keyBindings.find(axisName);
	if(key != InputBindings::keyBindings.end())
	{

		axisBindings[key->second].disconnect<T, function>(object);
	}
}

inline void InputController::UnBindAll() noexcept
{
	UnBindAllActions();
	UnBindAllAxes();
}

template<typename T>
inline void InputController::SwapInstance(T* oldInstance, T* newInstance) noexcept
{
	for(auto itKeyMap = actionBindings.begin(), end = actionBindings.end();
		itKeyMap != end; ++itKeyMap)
	{
		for(auto itStateSignal = itKeyMap->second.begin(), end2 = itKeyMap->second.end();
			itStateSignal != end2; ++itStateSignal)
		{
			itStateSignal->second.swapInstance(oldInstance, newInstance);
		}
	}

	for(auto it = axisBindings.begin(), end = axisBindings.end(); it != end; ++it)
	{
		it->second.swapInstance(oldInstance, newInstance);
	}
}

inline void InputController::PollInput() noexcept
{
	listener.PollInput(*this);
}

inline InputController::ButtonStateType operator+(InputController::ButtonState a) noexcept
{
	return static_cast<InputController::ButtonStateType>(a);
}
} // namespace Input
} // namespace Common

#endif // efc87351_063a_46c6_89c2_e30e682e5583
