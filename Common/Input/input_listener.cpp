#include "input_listener.hpp"

#include <cassert>
#include <mutex>

#include <EASTL/algorithm.h>

#include "input_controller.hpp"

namespace Common
{
namespace Input
{
InputListener::InputListener()
: inputBuffers()
, currentInputBuffer(&inputBuffers[0])
, pollInputBuffer(&inputBuffers.back())
, isCapturingInput()
, inputLock()
{
}

void InputListener::CaptureInput(s32 key, s32 scancode, s32 action, s32 mods)
{
	if(isCapturingInput.load(std::memory_order_acquire))
	{
		std::lock_guard<Common::Threading::spinlock> lock(inputLock);
		(void)lock;

		currentInputBuffer->emplace_back(eastl::make_tuple(key, scancode, action, mods));
	}
}

// No locking needed since not allowed to call during buffer swapping
void InputListener::PollInput(InputController &inputController) noexcept
{
	for(auto const &inputEntry : *pollInputBuffer)
	{
		inputController.ProcessKey(
			eastl::get<0>(inputEntry),
			eastl::get<1>(inputEntry),
			eastl::get<2>(inputEntry),
			eastl::get<3>(inputEntry));
	}
}

// Only allowed to call at the end of the frame, since this marks it (duh)
void InputListener::SwapBuffers() noexcept
{
	std::lock_guard<Common::Threading::spinlock> lock(inputLock);
	(void)lock;

	pollInputBuffer = currentInputBuffer;

	// Swap input buffers
	auto it = eastl::find(inputBuffers.begin(), inputBuffers.end(), *currentInputBuffer);
	std::size_t index = static_cast<std::size_t>(eastl::distance(inputBuffers.begin(), it));
	if(index == inputBuffers.size() - 1)
		index = 0;
	else
		++index;

	currentInputBuffer = &inputBuffers[index];
	currentInputBuffer->clear();
}
} // namespace Input
} // namespace Common
