#ifndef d64a7446_655f_469f_893a_321c7436b3ab
#define d64a7446_655f_469f_893a_321c7436b3ab

#include <EASTL/array.h>
#include <EASTL/tuple.h>
#include <EASTL/vector.h>

#include "../api.hpp"
#include "../types.hpp"
#include "../Threading/spinlock.hpp"

namespace Common
{
using namespace Types;

namespace Input
{
class InputController;

class COMMON_API InputListener
{
public:
	InputListener();
	InputListener(InputListener const &other) = default;
	InputListener(InputListener &&other) = default;
	~InputListener() noexcept = default;

	InputListener &operator=(InputListener const &other) = default;
	InputListener &operator=(InputListener &&other) = default;

	void CaptureInput(s32 key, s32 scancode, s32 action, s32 mods);
	void PollInput(InputController &inputController) noexcept;

	void SwapBuffers() noexcept;

	inline void ShouldCaptureInput(bool shouldCapture) noexcept;

	inline bool IsCapturingInput() const noexcept;

private:
	using InputBuffer = eastl::vector<eastl::tuple<s32, s32, s32, s32>>;

private:
	eastl::array<InputBuffer, 2> inputBuffers;
	InputBuffer *currentInputBuffer, *pollInputBuffer;
	std::atomic_bool isCapturingInput;
	Threading::spinlock inputLock;
};

inline void InputListener::ShouldCaptureInput(bool shouldCapture) noexcept
{
	isCapturingInput.store(shouldCapture, std::memory_order_release);

	if(!isCapturingInput.load(std::memory_order_acquire))
		currentInputBuffer->clear();
}

inline bool InputListener::IsCapturingInput() const noexcept
{
	return isCapturingInput.load(std::memory_order_acquire);
}
} // namespace Input
} // namespace Common

#endif // d64a7446_655f_469f_893a_321c7436b3ab
