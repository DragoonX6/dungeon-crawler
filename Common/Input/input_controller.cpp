#include "input_controller.hpp"

#include <EASTL/algorithm.h>
#include <EASTL/functional.h>
#include <EASTL/optional.h>
#include <iostream>

#include <GLFW/glfw3.h>

#include "../Windowing/window.hpp"
#include "../engine.hpp"
#include "input_listener.hpp"

namespace Common
{
using namespace Types;

namespace Input
{
eastl::unordered_map<eastl::string, s32> InputBindings::keyBindings;

InputController::InputController(InputListener &inputListener)
: actionBindings()
, axisBindings()
, listener(inputListener)
{
}

InputController::InputController(InputController &&other) noexcept
: actionBindings(eastl::move(other.actionBindings))
, axisBindings(eastl::move(other.axisBindings))
, listener(other.listener)
{
	other.actionBindings.clear();
	other.axisBindings.clear();
}

InputController::~InputController() noexcept
{
	actionBindings.clear();
	axisBindings.clear();
}

InputController &InputController::operator=(InputController &&other) noexcept
{
	actionBindings = eastl::move(other.actionBindings);
	axisBindings = eastl::move(other.axisBindings);

	other.actionBindings.clear();
	other.axisBindings.clear();

	return *this;
}

void InputController::UnBindActionState(
	eastl::string const &actionName,
	InputController::ButtonState state) noexcept
{
	auto key = InputBindings::keyBindings.find(actionName);
	if(key != InputBindings::keyBindings.end())
	{
		auto stateMap = actionBindings.find(key->second);
		if(stateMap != actionBindings.end())
		{
			auto stateMapElement = stateMap->second.find(+state);
			if(stateMapElement !=  stateMap->second.end())
			{
				stateMap->second.erase(stateMapElement);
			}
		}
	}
}

void InputController::UnBindActions(eastl::string const &actionName) noexcept
{
	auto key = InputBindings::keyBindings.find(actionName);
	if(key != InputBindings::keyBindings.end())
	{
		auto stateMap = actionBindings.find(key->second);
		if(stateMap != actionBindings.end())
		{
			actionBindings.erase(stateMap);
		}
	}
}

void InputController::UnBindAllActions() noexcept
{
	actionBindings.clear();
}

void InputController::UnBindAxes(eastl::string const &axisName) noexcept
{
	auto key = InputBindings::keyBindings.find(axisName);
	if(key != InputBindings::keyBindings.end())
	{
		auto functionMap = axisBindings.find(key->second);
		if(functionMap != axisBindings.end())
		{
			functionMap->second.removeAll();
		}
	}
}

void InputController::UnBindAllAxes() noexcept
{
	axisBindings.clear();
}

void InputController::BindKey(eastl::string const &keyName, Common::Types::s32 key) noexcept
{
	InputBindings::keyBindings[keyName] = key;
}

void InputController::ProcessKey(
	int key,
	int scancode,
	int action,
	int mods) noexcept
{
	(void)scancode;
	(void)mods;

	auto stateMap = actionBindings.find(key);
	if(stateMap != actionBindings.end())
	{
		ButtonState keyState;
		switch(action)
		{
			case GLFW_RELEASE:
			{
				keyState = ButtonState::released;
			}break;
			case GLFW_PRESS:
			{
				keyState = ButtonState::pressed;
			}break;
			case GLFW_REPEAT:
			{
				keyState = ButtonState::repeat;
			}break;
			default:
			{
				std::cerr << "Unknown action <" << action << ">" << std::endl;
				return;
			}
		}

		auto stateElement = stateMap->second.find(+keyState);
		if(stateElement != stateMap->second.end())
		{
			stateElement->second.emit();
		}
	}
}
} // namespace Input
} // namespace Common
