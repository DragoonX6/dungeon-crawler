#ifndef ce1185f9_9d80_455d_be4d_152f80ebbc87
#define ce1185f9_9d80_455d_be4d_152f80ebbc87

#include <EASTL/string.h>
#include <EASTL/unordered_map.h>
#include <EASTL/vector.h>
#include <EASTL/type_traits.h>

#include <nano_signal_slot.hpp>

#include "../api.hpp"
#include "../types.hpp"
#include "input_listener.hpp"

namespace Common
{
using namespace Types;

namespace Input
{
class COMMON_API InputBindings
{
public:
	// named keys, so instead of GLFW_KEY_W, we search
	static eastl::unordered_map<eastl::string, s32> keyBindings;
};

class COMMON_API InputController
{
public:
	enum class ButtonState: u8
	{
		pressed,
		released,
		repeat
	};

	using ButtonStateType = eastl::underlying_type<ButtonState>::type;
	using ButtonStateHashType = eastl::hash<ButtonStateType>;
	using ButtonStateMap = eastl::unordered_map<ButtonStateType,
		  Nano::Signal<void()>, ButtonStateHashType>;

public:
	explicit InputController(InputListener &inputListener);
	InputController(InputController const &other) = delete;
	InputController(InputController &&other) noexcept;
	~InputController() noexcept;

	InputController &operator=(InputController const &other) = delete;
	InputController &operator=(InputController &&other) noexcept;

	template<typename T, void (T::*function)() noexcept>
	inline void BindAction(
		eastl::string const &actionName,
		ButtonState state,
		T *const object) noexcept;

	template<typename T, void (T::*function)() noexcept>
	inline void UnBindActionFunction(
		eastl::string const &actionName,
		ButtonState state,
		T *const object) noexcept;

	void UnBindActionState(
		eastl::string const &actionName,
		ButtonState state) noexcept;
	void UnBindActions(eastl::string const &actionName) noexcept;
	void UnBindAllActions() noexcept;

	template<typename T, void (T::*function)() noexcept>
	inline void BindAxis(eastl::string const &axisName, T *const object) noexcept;

	template<typename T, void (T::*function)() noexcept>
	inline void UnBindAxisFunction(
		eastl::string const &axisName,
		T *const object) noexcept;

	void UnBindAxes(eastl::string const &axisName) noexcept;
	void UnBindAllAxes() noexcept;

	inline void UnBindAll() noexcept;

	template<typename T>
	inline void SwapInstance(T* oldInstance, T* newInstance) noexcept;

	static void BindKey(eastl::string const &keyName, s32 key) noexcept;

	void ProcessKey(int key, int scancode, int action, int mods) noexcept;

	inline void PollInput() noexcept;

private:
	eastl::unordered_map<s32, ButtonStateMap> actionBindings;
	eastl::unordered_map<s32, Nano::Signal<void(float)>> axisBindings;

	InputListener &listener;
};

inline InputController::ButtonStateType operator+(InputController::ButtonState a) noexcept;
} // namespace Input
} // namespace Common

#include "input_controller_impl.hpp"

#endif // ce1185f9_9d80_455d_be4d_152f80ebbc87
