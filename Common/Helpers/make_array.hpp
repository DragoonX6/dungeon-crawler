#ifndef f4665290_a818_41ea_8725_ea5f36943963
#define f4665290_a818_41ea_8725_ea5f36943963

/*
 * Credits to
 *    https://gist.github.com/lichray/6034753/337240ea9777c5118ba3430c5198c2d0d4f81a03
 * License unknown
 */

#include <EASTL/array.h>
#include <EASTL/functional.h>
#include <iso646.h>

namespace Common
{
namespace Helpers
{
namespace Internals
{
template <typename... T>
using common_type_t = typename eastl::common_type<T...>::type;

template <typename T>
using remove_cv_t = typename eastl::remove_cv<T>::type;

template <bool, typename T, typename... U>
struct lazy_conditional_c;

template <typename T>
struct lazy_conditional_c<true, T>
{
	using type = typename T::type;
};

template <typename T, typename U>
struct lazy_conditional_c<true, T, U>
{
	using type = typename T::type;
};

template <typename T, typename U>
struct lazy_conditional_c<false, T, U>
{
	using type = typename U::type;
};

template <typename V, typename T, typename... U>
using If = lazy_conditional_c<V::value, T, U...>;

template <typename V, typename T, typename... U>
using If_t = typename If<V, T, U...>::type;

template <typename T>
struct identity_of
{
	using type = T;
};

template <template <typename> class F, typename... T>
struct no_type : eastl::true_type {};

template <template <typename> class F, typename T1, typename... T2>
struct no_type<F, T1, T2...> :
eastl::integral_constant<bool, not F<T1>::value and no_type<F, T2...>::value>
{};

template <template <typename> class F, template <typename> class G>
struct composed
{
	template <typename T>
	using call = F<typename G<T>::type>;
};

template <typename T>
struct _is_reference_wrapper : eastl::false_type {};

template <typename T>
struct _is_reference_wrapper<eastl::reference_wrapper<T>> : eastl::true_type {};

template <typename T>
using is_reference_wrapper =
composed<_is_reference_wrapper, eastl::remove_cv>::call<T>;

template <size_t... I>
struct _indices {};

template <size_t N, size_t... I>
struct _build_indices : _build_indices<N - 1, N - 1, I...> {};

template <size_t... I>
struct _build_indices<0, I...> : _indices<I...> {};

template <typename T, size_t N, size_t... I>
constexpr auto _to_array(T (&arr)[N], _indices<I...>)
-> eastl::array<Internals::remove_cv_t<T>, N>
{
	return {{ arr[I]... }};
}
} // namespace Internals

template <typename V = void, typename... T>
constexpr auto make_array(T&&... t) -> eastl::array<Internals::If_t<eastl::is_void<V>,
		  Internals::If<Internals::no_type<Internals::composed<
		  Internals::is_reference_wrapper, eastl::remove_reference>::call, T...>,
		  eastl::common_type<T...>>, Internals::identity_of<V>>, sizeof...(T)>
{
	return {{ eastl::forward<T>(t)... }};
}

template <typename T, size_t N>
constexpr auto to_array(T (&arr)[N]) -> eastl::array<Internals::remove_cv_t<T>, N>
{
	return Internals::_to_array(arr, Internals::_build_indices<N>());
}
} // namespace Helpers
} // namespace Common

#endif // f4665290_a818_41ea_8725_ea5f36943963
