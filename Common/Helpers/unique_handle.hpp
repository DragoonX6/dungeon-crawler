#ifndef fbcf3a0d_9922_45d9_bdcd_c613ede9be90
#define fbcf3a0d_9922_45d9_bdcd_c613ede9be90

#include <EASTL/type_traits.h>
#include <EASTL/utility.h>

namespace Common
{
namespace Helpers
{
template<class T>
class UniqueHandle
{
	static_assert(eastl::is_integral<T>::value,
				  "UniqueHandle<T> only works for integral types.");

	static_assert(!eastl::is_pointer<T>::value,
				  "UniqueHandle<T> doesn't work with pointers.");
public:
	UniqueHandle() noexcept = default;
	UniqueHandle(UniqueHandle const &) = delete;

	UniqueHandle(UniqueHandle &&other) noexcept:
		handle(eastl::move(other.handle))
		{
			other.handle = 0;
		}

	~UniqueHandle() noexcept
	{
		handle = 0;
	}

	UniqueHandle &operator=(UniqueHandle const &) = delete;
	UniqueHandle &operator=(UniqueHandle &&other) noexcept
	{
		handle = eastl::move(other.handle);
		other.handle = 0;

		return *this;
	}

	T &operator*() noexcept
	{
		return handle;
	}

	T Get() const noexcept
	{
		return handle;
	}

private:
	T handle;
};
}
}

#endif // fbcf3a0d_9922_45d9_bdcd_c613ede9be90
