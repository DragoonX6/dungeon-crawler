#!/usr/bin/env python3
# encoding: utf-8

top = '.'
out = 'build'

def options(opt):
    opt.load('WaifuWaf', tooldir='.')

def configure(cfg):
    cfg.load('WaifuWaf', tooldir='.')

def build(bld):
    bld.recurse('ThirdParty/EASTL')
    bld.recurse('EASTLSupport')
    bld.recurse('Common')
    bld.recurse('Renderer')
    bld.recurse('Game')

